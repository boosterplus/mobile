import { AppRegistry } from 'react-native'
import App from './App'
import { name as appName } from './app.json'

// top of the tech optimisation
if (!__DEV__) {
  console.log = () => {}
  console.warn = () => {}
}

AppRegistry.registerComponent(appName, () => App)
