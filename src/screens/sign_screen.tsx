import React from 'react'
import styled from 'styled-components/native'
import { AppStackParamList } from '@src/navigators/app_navigator'
import { StackNavigationProp } from '@react-navigation/stack'
import { SignUpForm } from '@src/features/sign/sign_up_form'
import Logo from '@src/assets/logo.svg'
import { SignInForm } from '@src/features/sign/sign_in_form'
import { calendar } from '@src/lib/routes'

type NavigationProps = {
  navigation: StackNavigationProp<AppStackParamList, 'Sign'>
}

export const SignScreen = ({ navigation }: NavigationProps) => {
  const [signMode, setSignMode] = React.useState(0)
  return (
    <Wrapper>
      <LogoWrapper>
        <Logo />
      </LogoWrapper>
      {signMode === 0 ? (
        <SignInForm
          onSuccess={() => navigation.navigate(calendar)}
          onSignUpPress={() => setSignMode(1)}
        />
      ) : (
        <SignUpForm
          onSuccess={() => navigation.navigate(calendar)}
          onSignInPress={() => setSignMode(0)}
        />
      )}
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex: 1;
  background-color: white;
`

const LogoWrapper = styled.View`
  align-items: center;
  margin-top: 28px;
`
