import React from 'react'
import { TouchableOpacity, ScrollView, Platform, View } from 'react-native'
import { useFormik } from 'formik'
import styled from 'styled-components/native'
import CrossSVG from '@src/assets/cross.svg'
import { StackNavigationProp } from '@react-navigation/stack'
import { CalendarStackParamList } from '@src/navigators/calendar_navigator'
import { TextComponent } from '@src/shared/text'
import { InputSelect, InputSelectOption } from '@src/shared/input_select'
import { Input } from '@src/shared/input'
import Slider from '@react-native-community/slider'
import { createAction, getMissions } from '@src/api'
import { RouteProp } from '@react-navigation/native'
import DateTimePicker from '@react-native-community/datetimepicker'
import { DateTime } from 'luxon'
import Toast from 'react-native-simple-toast'
import { useActionStore } from '@src/store/actionsStore'
import { powerUpTypes, actionTypes } from '@src/lib/util'
import PlusSVG from '@src/assets/plusgray.svg'
import ArrowSVG from '@src/assets/arrow-top.svg'
import AddSVG from '@src/assets/add.svg'
import RemoveSVG from '@src/assets/remove.svg'
import { ResourceSelect } from '@src/features/actions/resource_select'

type NavigationProps = {
  navigation: StackNavigationProp<CalendarStackParamList, 'AddAction'>
  route: RouteProp<CalendarStackParamList, 'AddAction'>
}

const Repeat: { [key: string]: string } = {
  День: 'every_day',
  Неделю: 'every_week',
  Месяц: 'every_month',
}

export const AddActionScreen = ({ navigation, route }: NavigationProps) => {
  const scrollViewRef = React.useRef(null)
  const { dispatch, state } = useActionStore()
  const { date, fromYearView } = route.params
  const [visiblePickerIndex, setVisiblePickerIndex] = React.useState(-1)
  const [isRepeatVisible, setRepeatVisible] = React.useState(false)
  const currentDate = new Date()
  const formik = useFormik({
    initialValues: {
      actionType: actionTypes['Прокачивать'],
      powerUp: powerUpTypes[0],
      title: '',
      duration: 15,
      startDate: new Date(
        date.year,
        date.month - 1,
        date.day,
        currentDate.getHours(),
        Math.round(currentDate.getMinutes() / 10) * 10,
      ),
      repeatFrequency: '',
      repeatFrequencyCount: 1,
      repeatUntil: new Date(
        date.year,
        date.month - 1,
        date.day,
        currentDate.getHours(),
        Math.round(currentDate.getMinutes() / 10) * 10,
      ),
      mission_id: 'none',
    },
    onSubmit: values => {
      if (!values.title.length) {
        Toast.show('Описание действия не может быть пустым', 10)
        return
      }
      const task = {
        title: values.title,
        process: values.actionType.value,
        process_scope:
          values.actionType.value === 'level_up' ? values.powerUp.value : '',
        duration: values.duration,
        start_date: DateTime.fromJSDate(values.startDate).toISO(),
        frequency: values.repeatFrequency.length
          ? Repeat[values.repeatFrequency]
          : null,
        frequency_count: values.repeatFrequencyCount,
        end_repetition_date: values.repeatFrequency.length
          ? DateTime.fromJSDate(values.repeatUntil).toISO()
          : null,
        mission_id: values.mission_id === 'none' ? null : values.mission_id,
      }
      return createAction(task)
        .then(newAction => {
          dispatch({ type: 'addAction', payload: newAction })
          Toast.show('Действие добавлено!')
          navigation.goBack()
        })
        .then(() => getMissions())
        .then(missions => {
          dispatch({ type: 'loadNewMissions', payload: missions })
        })
        .catch(e => {
          Toast.show(e?.response?.data?.errors?.join('; '), 10)
        })
    },
  })

  const setStartDate = (_: Event, newTime: Date | undefined) => {
    if (Platform.OS === 'android') {
      setVisiblePickerIndex(-1)
    }
    formik.setFieldValue('startDate', newTime ?? formik.values.startDate)
  }

  const setRepeatUntil = (_: Event, newTime: Date | undefined) => {
    if (Platform.OS === 'android') {
      setVisiblePickerIndex(-1)
    }
    formik.setFieldValue('repeatUntil', newTime ?? formik.values.repeatUntil)
  }

  React.useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{ paddingLeft: 16 }}
        >
          <CrossSVG />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          onPress={formik.submitForm}
          style={{ paddingRight: 16 }}
        >
          <ReadyButton>Готово</ReadyButton>
        </TouchableOpacity>
      ),
    })
  }, [])

  return (
    <Wrapper>
      <ScrollView
        contentContainerStyle={{ paddingVertical: 16 }}
        ref={scrollViewRef}
      >
        <SubheaderText>Каким процессом мы занимаемся?</SubheaderText>
        <InputSelect
          style={{ marginBottom: 12 }}
          options={Object.keys(actionTypes)}
          selectedOption={formik.values.actionType.label}
          onOptionSelect={option =>
            formik.setFieldValue('actionType', actionTypes[option])
          }
        />
        {formik.values.actionType.value === 'level_up' ? (
          <>
            <SubheaderText>Что прокачиваем?</SubheaderText>
            <ResourceSelect
              onOptionSelect={option => formik.setFieldValue('powerUp', option)}
              selectedOption={formik.values.powerUp}
            />
          </>
        ) : null}
        <SubheaderText>Что нужно сделать?</SubheaderText>
        <Input
          style={{
            borderRadius: 0,
            borderColor: '#fff',
            height: 50,
            marginBottom: 12,
          }}
          placeholder={'Описание действия'}
          value={formik.values.title}
          onChangeText={value => formik.setFieldValue('title', value)}
        />
        <SubheaderText>Сроки действия</SubheaderText>
        {fromYearView ? (
          <DurationView
            activeOpacity={0.6}
            onPress={() =>
              visiblePickerIndex !== 0
                ? setVisiblePickerIndex(0)
                : setVisiblePickerIndex(-1)
            }
          >
            <DurationText>Старт</DurationText>
            <DurationText>
              {DateTime.fromJSDate(formik.values.startDate).toFormat(
                'dd.MM.yyyy',
              )}
            </DurationText>
          </DurationView>
        ) : null}
        {visiblePickerIndex === 0 ? (
          <DateTimePicker
            testID="datePicker"
            value={formik.values.startDate}
            mode={'date'}
            locale="ru-RU"
            textColor="#335366"
            display="default"
            onChange={setStartDate}
          />
        ) : null}
        <DurationView
          activeOpacity={0.6}
          onPress={() =>
            visiblePickerIndex !== 1
              ? setVisiblePickerIndex(1)
              : setVisiblePickerIndex(-1)
          }
        >
          <DurationText>Время</DurationText>
          <DurationText>
            {DateTime.fromJSDate(formik.values.startDate).toFormat('HH:mm')}
          </DurationText>
        </DurationView>
        {visiblePickerIndex === 1 ? (
          <DateTimePicker
            value={formik.values.startDate}
            locale="ru-RU"
            mode={'time'}
            textColor="#335366"
            minuteInterval={10}
            display="default"
            onChange={setStartDate}
          />
        ) : null}
        <DurationView
          activeOpacity={0.6}
          onPress={() =>
            visiblePickerIndex !== 2
              ? setVisiblePickerIndex(2)
              : setVisiblePickerIndex(-1)
          }
        >
          <DurationText>Продолжительность</DurationText>
          <DurationText>{formik.values.duration} минут</DurationText>
        </DurationView>
        {visiblePickerIndex === 2 ? (
          <Slider
            style={{ margin: 16 }}
            minimumValue={15}
            maximumValue={90}
            step={1}
            minimumTrackTintColor="#449e6f"
            maximumTrackTintColor="#449e6f"
            onValueChange={value => formik.setFieldValue('duration', value)}
          />
        ) : null}
        <SubheaderText style={{ marginTop: 12 }}>Повторение</SubheaderText>
        <DurationView
          activeOpacity={0.6}
          onPress={() => {
            if (!isRepeatVisible) {
              setRepeatVisible(true)
              formik.setFieldValue('repeatFrequency', null)
            } else {
              setRepeatVisible(false)
              formik.setFieldValue('repeatFrequency', '')
            }
          }}
        >
          <DurationText>
            Добавить повторение {isRepeatVisible ? 'каждый:' : ''}
          </DurationText>
          {isRepeatVisible ? (
            <ArrowSVG style={{ marginTop: 5 }} />
          ) : (
            <PlusSVG width={18} height={18} />
          )}
        </DurationView>
        {isRepeatVisible ? (
          <>
            <RepeatBlock>
              <RepeatView
                activeOpacity={0.6}
                onPress={() => formik.setFieldValue('repeatFrequency', null)}
              >
                <RepeatText
                  style={
                    formik.values.repeatFrequency === null
                      ? { fontWeight: 'bold' }
                      : {}
                  }
                >
                  Нет
                </RepeatText>
              </RepeatView>
              <RepeatView
                activeOpacity={0.6}
                onPress={() => formik.setFieldValue('repeatFrequency', 'День')}
              >
                <RepeatText
                  style={
                    formik.values.repeatFrequency === 'День'
                      ? { fontWeight: 'bold' }
                      : {}
                  }
                >
                  День
                </RepeatText>
              </RepeatView>
              <RepeatView
                activeOpacity={0.6}
                onPress={() =>
                  formik.setFieldValue('repeatFrequency', 'Неделю')
                }
              >
                <RepeatText
                  style={
                    formik.values.repeatFrequency === 'Неделю'
                      ? { fontWeight: 'bold' }
                      : {}
                  }
                >
                  Неделю
                </RepeatText>
              </RepeatView>
              <RepeatView
                activeOpacity={0.6}
                onPress={() => formik.setFieldValue('repeatFrequency', 'Месяц')}
              >
                <RepeatText
                  style={
                    formik.values.repeatFrequency === 'Месяц'
                      ? { fontWeight: 'bold' }
                      : {}
                  }
                >
                  Месяц
                </RepeatText>
              </RepeatView>
            </RepeatBlock>
            {formik.values.repeatFrequency ? (
              <View>
                <RepeatCountView>
                  <TouchableOpacity
                    style={{ width: '23.5%', paddingLeft: 16 }}
                    onPress={() =>
                      formik.setFieldValue(
                        'repeatFrequencyCount',
                        formik.values.repeatFrequencyCount !== 1
                          ? formik.values.repeatFrequencyCount - 1
                          : formik.values.repeatFrequencyCount,
                      )
                    }
                  >
                    <RemoveSVG />
                  </TouchableOpacity>
                  {formik.values.repeatFrequency === 'День' ? (
                    <RepeatCountText>
                      Повторять каждый
                      {formik.values.repeatFrequencyCount !== 1
                        ? ' ' + formik.values.repeatFrequencyCount
                        : ''}{' '}
                      день
                    </RepeatCountText>
                  ) : formik.values.repeatFrequency === 'Неделю' ? (
                    <RepeatCountText>
                      Повторять каждую
                      {formik.values.repeatFrequencyCount !== 1
                        ? ' ' + formik.values.repeatFrequencyCount
                        : ''}{' '}
                      неделю
                    </RepeatCountText>
                  ) : formik.values.repeatFrequency === 'Месяц' ? (
                    <RepeatCountText>
                      Повторять каждый
                      {formik.values.repeatFrequencyCount !== 1
                        ? ' ' + formik.values.repeatFrequencyCount
                        : ''}{' '}
                      месяц
                    </RepeatCountText>
                  ) : null}
                  <TouchableOpacity
                    style={{ width: '23.5%', paddingRight: 16 }}
                    onPress={() =>
                      formik.setFieldValue(
                        'repeatFrequencyCount',
                        formik.values.repeatFrequencyCount + 1,
                      )
                    }
                  >
                    <AddSVG style={{ marginLeft: 'auto' }} />
                  </TouchableOpacity>
                </RepeatCountView>
                <DurationView
                  style={{ marginTop: 5 }}
                  activeOpacity={0.6}
                  onPress={() =>
                    visiblePickerIndex !== 3
                      ? setVisiblePickerIndex(3)
                      : setVisiblePickerIndex(-1)
                  }
                >
                  <DurationText>Конец повторения</DurationText>
                  <DurationText>
                    {DateTime.fromJSDate(formik.values.repeatUntil).toFormat(
                      'dd.MM.yyyy',
                    )}
                  </DurationText>
                </DurationView>
              </View>
            ) : null}
            {visiblePickerIndex === 3 ? (
              <DateTimePicker
                testID="repeatUntil"
                value={formik.values.repeatUntil}
                mode={'date'}
                locale="ru-RU"
                textColor="#335366"
                display="default"
                onChange={setRepeatUntil}
              />
            ) : null}
          </>
        ) : null}
        <SubheaderText>Миссия</SubheaderText>
        <InputSelectOption
          onOptionSelect={value => formik.setFieldValue('mission_id', value)}
          options={state.missions
            .map(m => {
              return { label: m.attributes.title, value: m.id }
            })
            .concat([{ label: 'Выберите миссию', value: 'none' }])}
          selectedOption={formik.values.mission_id}
        />
      </ScrollView>
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex: 1;
  background-color: #f8fbfc;
`

const ReadyButton = styled(TextComponent)`
  font-weight: bold;
  font-size: 16px;
  color: #449e6f;
`

const SubheaderText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: rgba(51, 83, 102, 0.6);
  margin: 0 0 5px 16px;
`

const RepeatCountView = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 16px 0px;
  height: 50px;
  background-color: white;
  margin-top: 5px;
`

const RepeatCountText = styled(TextComponent)``

const DurationView = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: space-between;
  padding: 16px;
  height: 50px;
  background-color: white;
  margin-bottom: 5px;
`

const DurationText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #335366;
`

const RepeatBlock = styled.View`
  flex-direction: row;
  justify-content: space-between;
`

const RepeatView = styled.TouchableOpacity`
  background-color: white;
  padding: 12px 10px;
  width: 24%;
  justify-content: center;
  align-items: center;
`

const RepeatText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #335366;
`
