import React from 'react'
import { TouchableOpacity } from 'react-native'
import styled from 'styled-components/native'

import { StackNavigationProp } from '@react-navigation/stack'
import { CalendarStackParamList } from '@src/navigators/calendar_navigator'

import { TextComponent } from '@src/shared/text'
import { useActionStore } from '@src/store/actionsStore'

import EditSVG from '@src/assets/edit.svg'
import PlusSVG from '@src/assets/plusBlack.svg'

import * as routes from '@src/lib/routes'

type NavigationProps = {
  navigation: StackNavigationProp<CalendarStackParamList, 'FolderMissionsView'>
}

export const MissionsScreen = ({ navigation }: NavigationProps) => {
  const { state } = useActionStore()
  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity
          onPress={() => navigation.push(routes.addMission)}
          style={{ padding: 3 }}
        >
          <PlusSVG />
        </TouchableOpacity>
      ),
    })
  }, [navigation])
  return (
    <Container>
      {state.missions.map(mission => (
        <TouchableOpacity
          key={mission.id}
          activeOpacity={0.5}
          onPress={() => {
            navigation.push('AddMission', { mission_id: mission.id })
          }}
        >
          <Folder>
            <Title>{mission.attributes.title}</Title>
            <Extra>
              <EditSVG />
            </Extra>
          </Folder>
        </TouchableOpacity>
      ))}
    </Container>
  )
}

const Container = styled.ScrollView`
  flex: 1;
`

const Title = styled(TextComponent)`
  font-size: 18px;
`

const Extra = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-end;
`

const Folder = styled.View`
  background: #ffffff;
  height: 56px;
  margin: 3px 0;
  flex-direction: row;
  padding: 16px;
  align-items: center;
`
