import React from 'react'
import styled from 'styled-components/native'
import { YearViewHeader } from '@src/features/year_view/year_view_header'
import { YearView } from '@src/features/year_view/year_view'
import { DateTime } from 'luxon'
import { StackNavigationProp } from '@react-navigation/stack'
import { CalendarStackParamList } from '@src/navigators/calendar_navigator'
import { dayView, addAction, addMission } from '@src/lib/routes'
import { ButtonToolbar } from '@src/features/actions/button_toolbar'
import { DateObject } from '@src/typings/api'

type NavigationProps = {
  navigation: StackNavigationProp<CalendarStackParamList, 'YearView'>
}

export const YearViewScreen = ({ navigation }: NavigationProps) => {
  const [currentDate, setCurrentDate] = React.useState(DateTime.local())

  const pressDay = (day: DateObject) => {
    requestAnimationFrame(() => {
      navigation.navigate(dayView, {
        isActive: day.isActive,
        year: day.year,
        month: day.month,
        day: day.day,
      })
    })
  }

  return (
    <Wrapper>
      <YearViewHeader
        date={currentDate}
        onFolderPress={() => navigation.push('FolderView')}
      />
      <YearView
        date={currentDate}
        onMonthChange={index =>
          setCurrentDate(currentDate.plus({ month: index }))
        }
        onDayPress={pressDay}
      />
      <ButtonToolbar
        onAddActionPress={() =>
          navigation.navigate(addAction, {
            date: {
              year: currentDate.year,
              month: currentDate.month,
              day: currentDate.day,
            },
            fromYearView: true,
          })
        }
        onAddMissionPress={() =>
          navigation.navigate(addMission, {
            date: {
              year: currentDate.year,
              month: currentDate.month,
              day: currentDate.day,
            },
          })
        }
      />
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex: 1;
  background-color: white;
  position: relative;
`
