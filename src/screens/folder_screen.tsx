import React from 'react'
import { TouchableOpacity } from 'react-native'
import styled from 'styled-components/native'

import { StackNavigationProp } from '@react-navigation/stack'
import { CalendarStackParamList } from '@src/navigators/calendar_navigator'

import MissionSVG from '@src/assets/mission.svg'

import { TextComponent } from '@src/shared/text'
import { useActionStore } from '@src/store/actionsStore'

type NavigationProps = {
  navigation: StackNavigationProp<CalendarStackParamList, 'FolderView'>
}

export const FolderScreen = ({ navigation }: NavigationProps) => {
  const { state } = useActionStore()
  return (
    <Container>
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => navigation.push('FolderMissionsView')}
      >
        <Folder>
          <MissionSVG style={{ marginRight: 12 }} />
          <Title>Миссии</Title>
          <Extra>
            {state.missions.length > 0 && (
              <Title>({state.missions.length})</Title>
            )}
          </Extra>
        </Folder>
      </TouchableOpacity>
    </Container>
  )
}

const Container = styled.ScrollView`
  flex: 1;
`

const Title = styled(TextComponent)`
  font-size: 18px;
`

const Extra = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-end;
`

const Folder = styled.View`
  background: #ffffff;
  height: 56px;
  margin: 3px 0;
  flex-direction: row;
  padding: 16px;
  align-items: center;
`
