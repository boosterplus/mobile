import React from 'react'
import styled from 'styled-components/native'

import { PagerProvider, Pager, useIndex } from '@crowdlinker/react-native-pager'

import { TextComponent } from '@src/shared/text'
import { ButtonComponent } from '@src/shared/button'
import { PageIndicator } from '@src/features/sign/page_indicator'

import Logo from '@src/assets/logo.svg'
import WelcomeCurve0 from '@src/assets/WelcomePage/WelcomeCurve0.svg'
import WelcomeCurve1 from '@src/assets/WelcomePage/WelcomeCurve1.svg'
import WelcomeCurve2 from '@src/assets/WelcomePage/WelcomeCurve2.svg'
import WelcomeCurve3 from '@src/assets/WelcomePage/WelcomeCurve3.svg'

import WelcomeMain0 from '@src/assets/WelcomePage/WelcomeMain0.svg'
import WelcomeMain2 from '@src/assets/WelcomePage/WelcomeMain2.svg'
import WelcomeAction1 from '@src/assets/WelcomePage/WelcomeAction1.svg'
import WelcomeAction2 from '@src/assets/WelcomePage/WelcomeAction2.svg'
import WelcomeAction3 from '@src/assets/WelcomePage/WelcomeAction3.svg'

import ActionIcon from '@src/assets/WelcomePage/ActionIcon.svg'
import MissionIcon from '@src/assets/WelcomePage/MissionIcon.svg'
import { StackNavigationProp } from '@react-navigation/stack'
import { AppStackParamList } from '@src/navigators/app_navigator'
import { AsyncStorage, Platform } from 'react-native'

import { sign } from '@src/lib/routes'

type NavigationProps = {
  navigation: StackNavigationProp<AppStackParamList, 'WelcomeView'>
}

export const WelcomeScreen = ({ navigation }: NavigationProps) => {
  const [step, setStep] = React.useState(0)
  const completeTutorial = React.useCallback(() => {
    AsyncStorage.setItem('tutorialComplete', 'true').finally(() =>
      navigation.canGoBack() ? navigation.goBack() : navigation.replace(sign),
    )
  }, [])
  return (
    <Wrapper>
      <LogoWrapper>
        <Logo />
      </LogoWrapper>
      <PagerProvider onChange={setStep} activeIndex={step}>
        {/*  panProps={{ enabled: signUpStep === 0 ? true : false }} */}
        <Pager>
          <WelcomePage />
          <WelcomePage />
          <WelcomePage />
          <WelcomePage onEnd={completeTutorial} />
        </Pager>
      </PagerProvider>
      <PageIndicatorWrapper>
        <PageIndicator
          pageCount={4}
          selectedPageIndex={step}
          initPageIndex={0}
        />
      </PageIndicatorWrapper>
    </Wrapper>
  )
}

type WelcomePageProps = {
  onEnd?: () => void
}
const WelcomePage = React.memo(({ onEnd }: WelcomePageProps) => {
  const page = useIndex()
  return (
    <WelcomeView>
      <WelcomeText>{GetWelcomeText(page)}</WelcomeText>
      <WelcomeMain>{GetWelcomeMain(page, onEnd)}</WelcomeMain>
      <WelcomeArt>{GetWelcomeArt(page)}</WelcomeArt>
    </WelcomeView>
  )
})

const GetWelcomeArt = (page: number) => {
  switch (page) {
    default:
    case 0:
      return <WelcomeCurve0 preserveAspectRatio={'none'} width={'100%'} />
    case 1:
      return <WelcomeCurve1 preserveAspectRatio={'none'} width={'100%'} />
    case 2:
      return <WelcomeCurve2 preserveAspectRatio={'none'} width={'100%'} />
    case 3:
      return <WelcomeCurve3 preserveAspectRatio={'none'} width={'100%'} />
  }
}

const shadowStyle =
  Platform.OS === 'android'
    ? {
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        borderRadius: 10,
        shadowOpacity: 0.1,
        elevation: 45,
      }
    : {
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 11,
      }

const GetWelcomeMain = (page: number, onEnd?: () => void) => {
  switch (page) {
    default:
    case 0:
      return <WelcomeMain0 />
    case 1:
      return (
        <WelcomeMain1>
          <WelcomeAction1 style={shadowStyle} />
          <WelcomeAction2 style={shadowStyle} />
          <WelcomeAction3 style={shadowStyle} />
        </WelcomeMain1>
      )
    case 2:
      return <WelcomeMain2 />
    case 3:
      return (
        <WelcomeMain3>
          <ActionIcon style={shadowStyle} />
          <MainText>
            <LogoText>Действия</LogoText> – это задачи, которые формируют твое
            расписание.
          </MainText>
          <MissionIcon style={shadowStyle} />
          <MainText>
            Если планируешь несколько действий ведущих к одной цели, объединяй
            их в <LogoText>миссии</LogoText> для удобства управления и просмотра
          </MainText>
          {onEnd && (
            <ButtonComponent
              onPress={onEnd}
              primary
              text={'Начать'}
              style={{ marginTop: 50, width: 200 }}
            />
          )}
        </WelcomeMain3>
      )
  }
}

const GetWelcomeText = (page: number) => {
  switch (page) {
    default:
    case 0:
      return (
        <>
          Добро пожаловать в экосистему <LogoText>Booster+</LogoText>
        </>
      )
    case 1:
      return 'Управляй своей жизнью'
    case 2:
      return 'Прокачивай свои ресурсные сферы'
    case 3:
      return (
        <>
          Управляй своей жизнью, выполняя <LogoText>миссии</LogoText> и{' '}
          <LogoText>действия</LogoText>
        </>
      )
  }
}

const Wrapper = styled.View`
  background-color: #f5f5f5;
  flex: 1;
`

const WelcomeView = styled.View`
  flex: 1;
  height: 100%;
  justify-content: flex-start;
  padding-top: 70px;
`

const WelcomeText = styled(TextComponent)`
  margin: 5px 60px;
  text-align: center;
  font-size: 24px;
`
const LogoText = styled(TextComponent)`
  color: #449e6f;
  font-weight: bold;
`

const MainText = styled(TextComponent)`
  font-weight: bold;

  font-size: 16px;
  line-height: 24px;
  text-align: center;
`

const WelcomeArt = styled.View`
  position: absolute;
  z-index: 1;
  bottom: 0;
  left: 0;
  width: 100%;
  flex-direction: row;
`

const WelcomeMain = styled.View`
  position: relative;
  z-index: 990;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  padding-bottom: 180px;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`

const WelcomeMain1 = styled.View`
  justify-content: space-between;
`

const WelcomeMain3 = styled.View`
  padding: 0 60px;
  align-items: center;
`

const LogoWrapper = styled.View`
  align-items: center;
  width: 100%;
  margin-top: 28px;
  position: absolute;
  top: 0;
  height: 30px;
`

const PageIndicatorWrapper = styled.View`
  position: absolute;
  bottom: 0;
  width: 100%;
  left: 0;
  padding: 0 50px;
  align-items: center;
  padding-bottom: 50px;
  flex: 1;
`
