import React, { useMemo } from 'react'
import { TouchableOpacity, ScrollView, Platform, View } from 'react-native'
import { useFormik } from 'formik'
import styled from 'styled-components/native'
import { DateTime } from 'luxon'
import Toast from 'react-native-simple-toast'
import DateTimePicker from '@react-native-community/datetimepicker'
import BottomSheet from 'reanimated-bottom-sheet'
import { RouteProp } from '@react-navigation/native'

import { StackNavigationProp } from '@react-navigation/stack'
import { CalendarStackParamList } from '@src/navigators/calendar_navigator'

import { createMission, editMission, deleteMission } from '@src/api'

import { useActionStore } from '@src/store/actionsStore'
import { powerUpTypes, actionTypes } from '@src/lib/util'

import { TextComponent } from '@src/shared/text'
import { InputSelect } from '@src/shared/input_select'
import { Input } from '@src/shared/input'
import { InputBoolean } from '@src/shared/input_boolean'
import { InputButton } from '@src/shared/input_button'

import { ResourceSelect } from '@src/features/actions/resource_select'

import Plus from '@src/assets/plusBlack.svg'
import CrossSVG from '@src/assets/cross.svg'
import { ButtonComponent } from '@src/shared/button'

type NavigationProps = {
  navigation: StackNavigationProp<CalendarStackParamList, 'AddMission'>
  route: RouteProp<CalendarStackParamList, 'AddMission'>
}

export const AddMissionScreen = ({ navigation, route }: NavigationProps) => {
  const { dispatch, state } = useActionStore()
  const { actions } = state

  const existingMission = useMemo(
    () =>
      route.params?.mission_id
        ? state.missions.find(m => m.id === route.params?.mission_id ?? '')
        : undefined,
    [state.missions, route.params?.mission_id],
  )
  const isCreating = !existingMission

  const initialValues = useMemo(() => {
    const currentDate = new Date()
    const date = route.params?.date ?? DateTime.fromJSDate(currentDate)
    return {
      title: existingMission?.attributes.title ?? '',
      process:
        (existingMission &&
          Object.values(actionTypes).find(
            v => v.value === existingMission?.attributes.process,
          )) ??
        actionTypes['Прокачивать'],
      process_scope:
        powerUpTypes.find(
          t => t.value === existingMission?.attributes.process_scope,
        ) ?? powerUpTypes[0],
      start_date:
        (existingMission &&
          DateTime.fromISO(existingMission.attributes.start_date).toJSDate()) ??
        new Date(
          date.year,
          date.month - 1,
          date.day,
          currentDate.getHours(),
          Math.round(currentDate.getMinutes() / 10) * 10,
        ),
      without_deadline: existingMission
        ? existingMission.attributes.without_deadline
        : true,
      end_date:
        (existingMission?.attributes.end_date
          ? DateTime.fromISO(existingMission.attributes.end_date).toJSDate()
          : null) ??
        new Date(
          date.year,
          date.month - 1,
          date.day,
          currentDate.getHours(),
          Math.round(currentDate.getMinutes() / 10) * 10,
        ),
      task_ids:
        existingMission?.attributes.tasks.data.map(t => String(t.id)) ?? [],
    }
  }, [existingMission?.id])

  const formik = useFormik({
    initialValues,
    onSubmit: values => {
      if (!values.title.length) {
        Toast.show('Описание миссии не может быть пустым', 10)
        return Promise.resolve()
      }
      const mission = {
        title: values.title,
        process: values.process.value,
        process_scope:
          values.process.value === 'level_up' ? values.process_scope.value : '',
        start_date: DateTime.fromJSDate(values.start_date).toISO(),
        without_deadline: values.without_deadline,
        end_date: values.without_deadline
          ? undefined
          : DateTime.fromJSDate(values.start_date).toISO(),
        task_ids: values.task_ids,
      }
      return isCreating
        ? createMission(mission)
            .then(data => {
              if (data) {
                dispatch({ type: 'addMission', payload: data })
                Toast.show('Миссиия добавлена!')
                navigation.goBack()
              }
            })
            .catch(e => {
              Toast.show(e?.response?.data?.errors?.join?.('; ') ?? '', 10)
            })
        : editMission(existingMission?.id as string, mission)
            .then(data => {
              if (data) {
                dispatch({ type: 'editMission', payload: data })
                Toast.show('Миссиия обновлена!')
                navigation.goBack()
              }
            })
            .catch(e => {
              Toast.show(e?.response?.data?.errors?.join?.('; ') ?? '', 10)
            })
    },
  })

  const [visiblePickerIndex, setVisiblePickerIndex] = React.useState(-1)
  const setStartDate = (_: Event, newTime: Date | undefined) => {
    if (Platform.OS === 'android') {
      setVisiblePickerIndex(-1)
    }
    formik.setFieldValue('start_date', newTime ?? formik.values.start_date)
  }
  const setEndDate = (_: Event, newTime: Date | undefined) => {
    if (Platform.OS === 'android') {
      setVisiblePickerIndex(-1)
    }
    formik.setFieldValue('end_date', newTime ?? formik.values.start_date)
  }

  React.useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          disabled={formik.isSubmitting}
          onPress={() => navigation.goBack()}
          style={{ paddingLeft: 16 }}
        >
          <CrossSVG />
        </TouchableOpacity>
      ),
      headerTitle: () => (
        <TextComponent>
          {isCreating ? 'Создание миссии' : 'Редактирование миссии'}
        </TextComponent>
      ),
      headerRight: () => (
        <TouchableOpacity
          disabled={formik.isSubmitting}
          onPress={formik.submitForm}
          style={{ paddingRight: 16 }}
        >
          <ReadyButton>Готово</ReadyButton>
        </TouchableOpacity>
      ),
    })
  }, [formik.isSubmitting, isCreating])

  const [connected_actions, free_actions] = useMemo(() => {
    return [
      formik.values.task_ids
        .map(id => actions.find(action => String(action.id) === String(id)))
        .filter(action => action !== undefined),
      actions.filter(a => !formik.values.task_ids.includes(String(a.id))),
    ]
  }, [actions, formik.values.task_ids])
  const bottomSheetRef = React.useRef<BottomSheet | null>(null)
  return (
    <Wrapper>
      <ScrollView
        contentContainerStyle={{ paddingVertical: 16 }}
        onTouchStart={() => bottomSheetRef.current?.snapTo(1)}
      >
        <SubheaderText>Название миссии?</SubheaderText>
        <Input
          style={{
            borderRadius: 0,
            borderColor: '#fff',
            height: 50,
            marginBottom: 12,
          }}
          value={formik.values.title}
          onChangeText={value => formik.setFieldValue('title', value)}
        />
        <SubheaderText>Каким процессом мы занимаемся?</SubheaderText>
        <InputSelect
          style={{ marginBottom: 12 }}
          options={Object.keys(actionTypes)}
          selectedOption={formik.values.process.label}
          onOptionSelect={option =>
            formik.setFieldValue('process', actionTypes[option])
          }
        />
        {formik.values.process.value === 'level_up' ? (
          <>
            <SubheaderText>Что прокачиваем?</SubheaderText>
            <ResourceSelect
              onOptionSelect={option =>
                formik.setFieldValue('process_scope', option)
              }
              selectedOption={formik.values.process_scope}
            />
          </>
        ) : null}

        <SubheaderText>Сроки действия</SubheaderText>

        <DurationView
          activeOpacity={0.6}
          onPress={() =>
            visiblePickerIndex !== 0
              ? setVisiblePickerIndex(0)
              : setVisiblePickerIndex(-1)
          }
        >
          <DurationText>Старт</DurationText>
          <DurationText>
            {DateTime.fromJSDate(formik.values.start_date).toFormat(
              'dd.MM.yyyy',
            )}
          </DurationText>
        </DurationView>

        {visiblePickerIndex === 0 ? (
          <DateTimePicker
            testID="datePicker"
            value={formik.values.start_date}
            mode={'date'}
            locale="ru-RU"
            textColor="#335366"
            display="default"
            onChange={setStartDate}
          />
        ) : null}
        {!formik.values.without_deadline && (
          <>
            <DurationView
              activeOpacity={0.6}
              onPress={() =>
                visiblePickerIndex !== 1
                  ? setVisiblePickerIndex(1)
                  : setVisiblePickerIndex(-1)
              }
            >
              <DurationText>Дедлайн</DurationText>
              <DurationText>
                {DateTime.fromJSDate(formik.values.end_date).toFormat(
                  'dd.MM.yyyy',
                )}
              </DurationText>
            </DurationView>
            {visiblePickerIndex === 1 && (
              <DateTimePicker
                minimumDate={formik.values.start_date}
                testID="datePicker"
                value={formik.values.end_date}
                mode={'date'}
                locale="ru-RU"
                textColor="#335366"
                display="default"
                onChange={setEndDate}
              />
            )}
          </>
        )}
        <InputBoolean
          label={'Без дедлайна'}
          onChange={val => formik.setFieldValue('without_deadline', val)}
          value={formik.values.without_deadline}
        />
        <SubheaderText>Привязать действие</SubheaderText>
        {connected_actions.map(
          action =>
            action && (
              <InputButton
                key={action.id}
                label={action.attributes.title ?? ''}
                onPress={() => {
                  const index = formik.values.task_ids.findIndex(
                    id => id === String(action?.id),
                  )
                  if (index > -1) {
                    formik.values.task_ids.splice(index, 1)
                    formik.setFieldValue('task_ids', [
                      ...formik.values.task_ids,
                    ])
                  }
                }}
              >
                <DurationText>
                  {DateTime.fromISO(action.attributes.start_date).toFormat('f')}
                </DurationText>
              </InputButton>
            ),
        )}
        <InputButton
          label={'Добавить действие'}
          onPress={() => bottomSheetRef.current?.snapTo(0)}
        >
          <Plus fill={'black'} />
        </InputButton>
        {existingMission && (
          <ButtonContainer>
            <ButtonComponent
              text={'Удалить миссию'}
              fullWidth
              danger
              style={{ marginTop: 20 }}
              textStyle={{ color: '#FF6464' }}
              onPress={() => {
                formik.setSubmitting(true)
                deleteMission(existingMission.id)
                  .then(() => {
                    dispatch({
                      type: 'deleteMission',
                      payload: { id: existingMission.id },
                    })
                    navigation.goBack()
                  })
                  .finally(() => formik.setSubmitting(false))
              }}
            />
          </ButtonContainer>
        )}
      </ScrollView>
      <BottomSheet
        initialSnap={1}
        snapPoints={['50%', 0]}
        ref={bottomSheetRef}
        renderHeader={() => (
          <SheetHeader>
            <SubheaderText>Привязать действие</SubheaderText>
          </SheetHeader>
        )}
        enabledInnerScrolling
        renderContent={() => (
          <View
            style={{
              height: '100%',
              backgroundColor: '#fff',
              alignItems: 'center',
            }}
          >
            {free_actions.map(action => (
              <InputButton
                key={action.id}
                label={action.attributes.title ?? ''}
                onPress={() => {
                  formik.setFieldValue('task_ids', [
                    ...formik.values.task_ids,
                    String(action.id),
                  ])
                  bottomSheetRef.current?.snapTo(1)
                }}
              >
                <DurationText>
                  {DateTime.fromISO(action.attributes.start_date).toFormat('f')}
                </DurationText>
              </InputButton>
            ))}
            <InputButton
              label={'Создать действие'}
              onPress={() => {
                navigation.push('AddAction', {
                  date: DateTime.fromJSDate(initialValues.start_date),
                })
              }}
            >
              <Plus />
            </InputButton>
            {free_actions.length === 0 && (
              <TextComponent style={{ marginTop: 16 }}>
                Нет действий
              </TextComponent>
            )}
          </View>
        )}
      />
    </Wrapper>
  )
}

const ButtonContainer = styled.View`
  margin-left:16px;
  margin-right:16px;
`

const Wrapper = styled.View`
  flex: 1;
  background-color: #f8fbfc;
`

const ReadyButton = styled(TextComponent)`
  font-weight: bold;
  font-size: 16px;
  color: #449e6f;
`

const SubheaderText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: rgba(51, 83, 102, 0.6);
  margin: 0 0 5px 16px;
`

const DurationView = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: space-between;
  padding: 16px;
  height: 50px;
  background-color: white;
  margin-bottom: 5px;
`

const DurationText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #335366;
`

const SheetHeader = styled.View`
    width:100%;
    border-color: #c5c7c9
    border-top-width: 2px;
    border-left-width:1px;
    border-right-width: 1px;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background: #F8FAFC;
    justify-content:center;
    align-items: center;
    height: 40px;
         
`
