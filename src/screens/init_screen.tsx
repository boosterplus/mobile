import React, { useEffect, useState } from 'react'
import { Loader } from '@src/shared/loader'
import { StackNavigationProp } from '@react-navigation/stack'
import { RootStackParamList } from '@src/navigators/root_navigator'
import { setApiToken } from '@src/api/index'

import { app } from '@src/lib/routes'
import AsyncStorage from '@react-native-community/async-storage'

type NavigationProps = {
  navigation: StackNavigationProp<RootStackParamList, 'Init'>
}

export const InitScreen = ({ navigation }: NavigationProps) => {
  const [isTokenLoaded, loadToken] = useState<boolean | undefined>(undefined)
  const [isTutorialComplete, setTutorialComplete] = useState<
    boolean | undefined
  >(undefined)
  useEffect(() => {
    AsyncStorage.multiGet(['token', 'tutorialComplete']).then(result => {
      const userToken = result[0][1]
      const tutorialComplete = (result[1][1] ?? '') === 'true'
      if (userToken?.length) {
        setApiToken(userToken)
      }
      loadToken(!!userToken?.length)
      setTutorialComplete(tutorialComplete)
    })
  }, [])

  useEffect(() => {
    if (isTokenLoaded !== undefined) {
      navigation.replace(app, {
        hasToken: isTokenLoaded,
        tutorialComplete: isTutorialComplete ?? false,
      })
    }
  }, [isTokenLoaded])

  return <Loader />
}
