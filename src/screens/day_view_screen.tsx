import React, { useCallback } from 'react'
import styled from 'styled-components/native'
import { DateTime } from 'luxon'
import { DayView } from '@src/features/day_view/day_view'
import { ButtonToolbar } from '@src/features/actions/button_toolbar'
import { StackNavigationProp } from '@react-navigation/stack'
import { CalendarStackParamList } from '@src/navigators/calendar_navigator'
import { addAction, editAction, addMission } from '@src/lib/routes'
import { DayViewHeader } from '@src/features/day_view/day_view_header'
import { RouteProp } from '@react-navigation/native'
import BottomSheet from 'reanimated-bottom-sheet'
import { ActionObject } from '@src/typings/api'
import { ActionHeader } from '@src/features/actions/action_header'

type NavigationProps = {
  navigation: StackNavigationProp<CalendarStackParamList, 'DayView'>
  route: RouteProp<CalendarStackParamList, 'DayView'>
}

export const DayViewScreen = ({ navigation, route }: NavigationProps) => {
  const [currentAction, setCurrentAction] = React.useState<ActionObject | null>(
    null,
  )
  const bottomSheetRef = React.useRef(null)
  const { params: props } = route
  const currentDate = React.useMemo(() => {
    return DateTime.local(props.year, props.month, props.day)
  }, [route.params])

  const clickCalendar = useCallback(() => {
    requestAnimationFrame(() => {
      navigation.goBack()
    })
  }, [navigation])
  const clickFolder = useCallback(() => {
    requestAnimationFrame(() => {
      navigation.push('FolderView')
    })
  }, [navigation])

  return (
    <Wrapper>
      <DayViewHeader
        date={currentDate}
        onCalendarClick={clickCalendar}
        onFolderPress={clickFolder}
      />
      <DayView
        date={currentDate}
        isActive={props.isActive}
        onActionPress={action => navigation.navigate(editAction, { action })}
        onActionLongPress={action => {
          setCurrentAction(action)
          const ref = bottomSheetRef.current as any
          ref.snapTo(0)
        }}
      />
      <ButtonToolbar
        onAddMissionPress={() => {
          navigation.navigate(addMission, {
            date: {
              year: currentDate.year,
              month: currentDate.month,
              day: currentDate.day,
            },
          })
        }}
        onAddActionPress={() =>
          navigation.navigate(addAction, {
            date: {
              year: currentDate.year,
              month: currentDate.month,
              day: currentDate.day,
            },
          })
        }
      />
      {currentAction ? (
        <BottomSheet
          borderRadius={8}
          ref={bottomSheetRef}
          initialSnap={1}
          snapPoints={['25%',0]}
          renderContent={() => (
            <ActionHeader
              action={currentAction}
              onEditClick={() => {
                const ref = bottomSheetRef.current as any
                ref.snapTo(1)
                navigation.navigate(editAction, { action: currentAction })
              }}
              onClose={() => {
                const ref = bottomSheetRef.current as any
                ref.snapTo(1)
              }}
            />
          )}
        />
      ) : null}
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex: 1;
  background-color: white;
  position: relative;
`
