import React from 'react'
import { TouchableOpacity, ScrollView, Platform } from 'react-native'
import { useFormik } from 'formik'
import styled from 'styled-components/native'
import CrossSVG from '@src/assets/cross.svg'
import { StackNavigationProp } from '@react-navigation/stack'
import { CalendarStackParamList } from '@src/navigators/calendar_navigator'
import { TextComponent } from '@src/shared/text'
import { InputSelect, InputSelectOption } from '@src/shared/input_select'
import { Input } from '@src/shared/input'
import Slider from '@react-native-community/slider'
import { RouteProp } from '@react-navigation/native'
import DateTimePicker from '@react-native-community/datetimepicker'
import { DateTime } from 'luxon'
import Toast from 'react-native-simple-toast'
import { useActionStore } from '@src/store/actionsStore'
import { powerUpTypes, actionTypes } from '@src/lib/util'
import { ResourceSelect } from '@src/features/actions/resource_select'
import CheckSVG from '@src/assets/check.svg'
import { editAction, getMissions } from '@src/api'

type NavigationProps = {
  navigation: StackNavigationProp<CalendarStackParamList, 'EditAction'>
  route: RouteProp<CalendarStackParamList, 'EditAction'>
}

const Repeat: { [key: string]: string } = {
  every_day: 'день',
  every_week: 'неделю',
  every_month: 'месяц',
}

export const EditActionScreen = ({ navigation, route }: NavigationProps) => {
  const { action } = route.params
  const scrollViewRef = React.useRef(null)
  const { dispatch, state } = useActionStore()
  const mission = React.useMemo(() => {
    return state.missions.find(m => {
      return m.attributes.tasks.data.find(a => a.id === action.id)
    })
  }, [action, state.missions])
  const [visiblePickerIndex, setVisiblePickerIndex] = React.useState(-1)
  const formik = useFormik({
    initialValues: {
      actionType: actionTypes[action.attributes.process],
      powerUp: powerUpTypes.find(
        powerup => powerup.label === action.attributes.process_scope,
      ),
      title: action.attributes.title,
      duration: action.attributes.duration,
      startDate: DateTime.fromISO(action.attributes.start_date).toJSDate(),
      status: action.attributes.status,
      mission_id: mission?.id ?? 'none',
    },
    onSubmit: values => {
      if (!values.title.length) {
        Toast.show('Описание действия не может быть пустым', 10)
        return
      }

      const task = {
        title: values.title,
        process: values.actionType.value,
        process_scope:
          values.actionType.value === 'level_up' ? values.powerUp?.value : '',
        duration: values.duration,
        start_date: DateTime.fromJSDate(values.startDate).toISO(),
        status: values.status,
        mission_id: values.mission_id === 'none' ? null : values.mission_id,
      }
      return editAction(action.id, task)
        .then(editedAction => {
          dispatch({ type: 'editAction', payload: editedAction })
          Toast.show('Действие сохранено!')
          navigation.goBack()
        })
        .then(() => getMissions())
        .then(missions => {
          dispatch({ type: 'loadNewMissions', payload: missions })
        })
        .catch(e => {
          Toast.show(e?.response?.data?.errors?.join('; '), 10)
        })
    },
  })
  const setStartDate = (_: Event, newTime: Date | undefined) => {
    if (Platform.OS === 'android') {
      setVisiblePickerIndex(-1)
    }
    formik.setFieldValue('startDate', newTime ?? formik.values.startDate)
  }

  const onActionStatusClick = () => {
    const status =
      formik.values.status === 'completed' ? 'uncompleted' : 'completed'
    formik.setFieldValue('status', status)
  }

  React.useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{ paddingLeft: 16 }}
        >
          <CrossSVG />
        </TouchableOpacity>
      ),
      headerRight: () => (
        <TouchableOpacity
          onPress={formik.submitForm}
          style={{ paddingRight: 16 }}
        >
          <ReadyButton>Готово</ReadyButton>
        </TouchableOpacity>
      ),
    })
  }, [])

  return (
    <Wrapper>
      <ScrollView
        contentContainerStyle={{ paddingVertical: 16 }}
        ref={scrollViewRef}
        // onContentSizeChange={() => {
        //   const ref = scrollViewRef.current as any
        //   ref.scrollToEnd({ animated: true })
        // }}
      >
        <SubheaderText>Описание</SubheaderText>
        <Input
          style={{
            borderRadius: 0,
            borderColor: '#fff',
            height: 50,
          }}
          placeholder={'Описание действия'}
          value={formik.values.title}
          onChangeText={value => formik.setFieldValue('title', value)}
        />
        <SubheaderText>Статус</SubheaderText>
        <StatusView>
          <StatusText>
            {formik.values.status === 'completed'
              ? 'Выполнено'
              : 'Не выполнено'}
          </StatusText>
          <TouchableOpacity activeOpacity={0.6} onPress={onActionStatusClick}>
            <StatusCheckbox>
              {formik.values.status === 'completed' ? <CheckSVG /> : null}
            </StatusCheckbox>
          </TouchableOpacity>
        </StatusView>
        <SubheaderText>Процесс</SubheaderText>
        <InputSelect
          style={{ marginBottom: 12 }}
          options={Object.keys(actionTypes)}
          selectedOption={formik.values.actionType.label}
          onOptionSelect={option =>
            formik.setFieldValue('actionType', actionTypes[option])
          }
        />
        {formik.values.actionType.value === 'level_up' &&
        formik.values.powerUp ? (
          <>
            <SubheaderText>Ресурсная сфера</SubheaderText>
            <ResourceSelect
              onOptionSelect={option => formik.setFieldValue('powerUp', option)}
              selectedOption={formik.values.powerUp}
            />
          </>
        ) : null}
        <SubheaderText>Сроки действия</SubheaderText>
        <DurationView
          activeOpacity={0.6}
          onPress={() =>
            visiblePickerIndex !== 0
              ? setVisiblePickerIndex(0)
              : setVisiblePickerIndex(-1)
          }
        >
          <DurationText>Старт</DurationText>
          <DurationText>
            {DateTime.fromJSDate(formik.values.startDate).toFormat(
              'dd.MM.yyyy',
            )}
          </DurationText>
        </DurationView>
        {visiblePickerIndex === 0 ? (
          <DateTimePicker
            testID="datePicker"
            value={formik.values.startDate}
            mode={'date'}
            locale="ru-RU"
            textColor="#335366"
            display="default"
            onChange={setStartDate}
          />
        ) : null}
        <DurationView
          activeOpacity={0.6}
          onPress={() =>
            visiblePickerIndex !== 1
              ? setVisiblePickerIndex(1)
              : setVisiblePickerIndex(-1)
          }
        >
          <DurationText>Время</DurationText>
          <DurationText>
            {DateTime.fromJSDate(formik.values.startDate).toFormat('HH:mm')}
          </DurationText>
        </DurationView>
        {visiblePickerIndex === 1 ? (
          <DateTimePicker
            value={formik.values.startDate}
            locale="ru-RU"
            mode={'time'}
            textColor="#335366"
            minuteInterval={10}
            display="default"
            onChange={setStartDate}
          />
        ) : null}
        <DurationView
          activeOpacity={0.6}
          onPress={() =>
            visiblePickerIndex !== 2
              ? setVisiblePickerIndex(2)
              : setVisiblePickerIndex(-1)
          }
        >
          <DurationText>Продолжительность</DurationText>
          <DurationText>{formik.values.duration} минут</DurationText>
        </DurationView>
        {visiblePickerIndex === 2 ? (
          <Slider
            style={{ margin: 16 }}
            minimumValue={15}
            maximumValue={90}
            step={1}
            minimumTrackTintColor="#449e6f"
            maximumTrackTintColor="#449e6f"
            onValueChange={value => formik.setFieldValue('duration', value)}
          />
        ) : null}
        {action.attributes.frequency ? (
          <DurationView activeOpacity={1}>
            <DurationText>Повторение</DurationText>
            <DurationText>
              Раз в {Repeat[action.attributes.frequency]}
            </DurationText>
          </DurationView>
        ) : null}
        {action.attributes.end_repetition_date ? (
          <DurationView activeOpacity={1}>
            <DurationText>Конец повторения</DurationText>
            <DurationText>
              {DateTime.fromISO(action.attributes.end_repetition_date).toFormat(
                'dd.MM.yyyy',
              )}
            </DurationText>
          </DurationView>
        ) : null}
        <SubheaderText>Миссия</SubheaderText>
        <InputSelectOption
          onOptionSelect={value => formik.setFieldValue('mission_id', value)}
          options={state.missions
            .map(m => {
              return { label: m.attributes.title, value: m.id }
            })
            .concat([{ label: 'Выберите миссию', value: 'none' }])}
          selectedOption={formik.values.mission_id}
        />
      </ScrollView>
    </Wrapper>
  )
}

const Wrapper = styled.View`
  flex: 1;
  background-color: #f8fbfc;
`

const ReadyButton = styled(TextComponent)`
  font-weight: bold;
  font-size: 16px;
  color: #449e6f;
`

const SubheaderText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: rgba(51, 83, 102, 0.6);
  margin: 12px 0 5px 16px;
`

const DurationView = styled.TouchableOpacity`
  flex-direction: row;
  justify-content: space-between;
  padding: 16px;
  height: 50px;
  background-color: white;
  margin-bottom: 5px;
`

const DurationText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #335366;
`

const StatusView = styled.View`
  padding: 16px;
  background-color: white;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const StatusText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #335366;
`

const StatusCheckbox = styled.View`
  border-width: 1.5px;
  border-color: #449e6f;
  min-width: 22px;
  min-height: 22px;
  width: 22px;
  height: 22px;
  border-radius: 22px;
  justify-content: center;
  align-items: center;
`
