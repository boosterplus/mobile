import React from 'react'
import { View, Animated, TouchableOpacity, ScrollView } from 'react-native'
import styled from 'styled-components/native'
import { TextComponent } from '@src/shared/text'
import { DateTime } from 'luxon'
import { ActionObject } from '@src/typings/api'
import { useActionStore } from '@src/store/actionsStore'
import { actionTypes } from '@src/lib/util'
import {
  PinchGestureHandler,
  PanGestureHandler,
  FlingGestureHandler,
  NativeViewGestureHandler,
} from 'react-native-gesture-handler'

import { usePinch } from '@src/lib/hooks'

type DayProps = {
  date: DateTime
  isActive?: boolean
  onActionPress: (action: ActionObject) => void
  onActionLongPress: (action: ActionObject) => void
}

// type HourItem = {
//   title: string
//   actions: ActionObject[] | null
// }

export const DayView = ({
  date,
  isActive,
  onActionPress,
  onActionLongPress,
}: DayProps) => {
  // const mutableStateRef = React.useRef({
  //   currentHeight: 0,
  //   previousHeight: 0,
  //   currentPosition: 0
  // })

  const scrollViewRef = React.useRef(null)

  const { state } = useActionStore()
  const { onPinchEvent, onPinchStateChange, scale, pinchPosition } = usePinch(1)
  const filteredActions = React.useMemo(() => {
    if (!state.actions.length) {
      return []
    } else {
      return state.actions.filter(action => {
        return DateTime.fromISO(action.attributes.start_date).hasSame(
          date,
          'day',
        )
      })
    }
  }, [state.actions])

  const weekDay = React.useMemo(() => {
    return date.setLocale('ru-RU').weekdayShort
  }, [date])

  const hourSegments = React.useMemo(() => {
    const segments = []
    const startTime = date.set({ hour: 0, minute: 0 })
    for (let i = 0; i < 24; i++) {
      const newTime = startTime.set({ hour: 1 * i })
      segments.push({
        title: newTime.toFormat('HH:mm'),
        actions: filteredActions.filter(action =>
          DateTime.fromISO(action.attributes.start_date).hasSame(
            newTime,
            'hour',
          ),
        ),
      })
    }
    return segments
  }, [date, filteredActions])

  // const handleScroll = (event: any) => {
  //   currentPosition = event.nativeEvent.contentOffset.y;
  // }

  return (
    <Container>
      <Header>
        <Day>
          <DayNumberView
            style={isActive ? { backgroundColor: '#449E6F' } : undefined}
          >
            <DayNumber style={isActive ? { color: 'white' } : undefined}>
              {date.day}
            </DayNumber>
          </DayNumberView>
          <Weekday style={isActive ? { marginLeft: 5 } : undefined}>
            {weekDay}
          </Weekday>
        </Day>
        <TasksText>
          {filteredActions.length > 0
            ? `действий: ${filteredActions.length}`
            : 'ничего не запланировано'}
        </TasksText>
      </Header>
      <PinchGestureHandler
        onGestureEvent={onPinchEvent}
        onHandlerStateChange={onPinchStateChange}
      >
        <NativeViewGestureHandler>
          <ScrollView
            ref={scrollViewRef}
          //Zoom

          // onLayout={ev => {
          //   const fixedContentHeight = ev.nativeEvent.layout.height;
          //   previousHeight = fixedContentHeight;
          // }}
          // onContentSizeChange={(w, h) => {
          //   currentHeight = h
          //   const ref = scrollViewRef.current as any
          //   var heightDeviation = currentHeight - previousHeight
          //   let positionRatio = (currentPosition + pinchPosition.__getValue()) / currentHeight
          //   previousHeight = currentHeight
          //   currentPosition = currentPosition + heightDeviation * positionRatio
          //   ref.scrollTo({
          //     x: 0,
          //     y: currentPosition,
          //     animated: false
          //   })
          // }}
          // onScrollBeginDrag={handleScroll}
          >
            <HourGridView>
              {hourSegments?.map(hourSegment => {
                return (
                  <HourGridBlock
                    key={hourSegment.title}
                    style={{
                      // height: scale.interpolate({
                      //   inputRange: [0.2, 2],
                      //   outputRange: [24, 240],
                      //   extrapolate: 'clamp',
                      // }),
                      height: 100,
                      flexDirection: 'row',
                      top: 15,
                    }}
                  >
                    <HourGrid>
                      <Hour>{hourSegment.title}</Hour>
                      <Grid />
                    </HourGrid>
                  </HourGridBlock>
                )
              })}
            </HourGridView>
            <TaskGridView
              style={{ top: 15 }}>
              {hourSegments?.map(hourSegment => {
                return (
                  <TaskGridBlock
                    style={{
                      // height: scale.interpolate({
                      //   inputRange: [0.2, 2],
                      //   outputRange: [24, 240],
                      //   extrapolate: 'clamp',
                      // }),
                      height: 100,
                      flexDirection: 'row',
                      //minHeight: 24,
                    }}
                    key={hourSegment.title}
                  >
                    <TaskView>
                      {hourSegment.actions?.map(action => {
                        return (
                          <TouchableOpacity
                            activeOpacity={0.7}
                            key={action.id}
                            onLongPress={() => onActionLongPress(action)}
                            onPress={() => onActionPress(action)}
                            style={{
                              position: 'absolute',
                              height: `${action.attributes.duration / 0.6}%`,
                              width: '100%',
                              paddingLeft: 76,
                              flexDirection: 'row',
                              top: DateTime.fromISO(action.attributes.start_date).minute / 0.6,
                              left: 0,
                              alignItems: 'center',
                            }}
                          >
                            <View
                              style={{
                                width: '100%',
                                height: '100%',
                                borderColor: '#F2F5F5',
                                borderRadius: 4,
                                borderTopWidth: 1,
                                borderBottomWidth: 1,
                                backgroundColor:
                                  actionTypes[action.attributes.process]
                                    ?.color ?? '#449E6F',
                              }}
                            />
                            <TaskTitle
                            // style={{
                            //   opacity: scale.interpolate({
                            //     inputRange: [0.2, 2],
                            //     outputRange: [24, 240],
                            //     extrapolate: 'clamp',
                            //   }).interpolate({
                            //     inputRange: [action.attributes.duration/0.6, action.attributes.duration/0.6],
                            //     outputRange: [0, 1],
                            //     extrapolate: 'clamp',
                            //   }),
                            // }}
                            >
                              {action.attributes.title}
                            </TaskTitle>
                          </TouchableOpacity>
                        )
                      })}
                    </TaskView>
                  </TaskGridBlock>
                )
              })}
            </TaskGridView>
          </ScrollView>
        </NativeViewGestureHandler>
      </PinchGestureHandler>
    </Container>
  )
}

const Container = styled.View`
  flex: 1;
`

const Header = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: #f2f5f5;
  padding: 8px 15px;
`

const DayNumberView = styled.View`
  padding: 5px;
  height: 29px;
  width: 29px;
  border-radius: 29px;
  align-items: center;
`

const DayNumber = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  text-align: center;
  color: #5c7585;
`

const Day = styled.View`
  flex-direction: row;
  align-items: center;
`

const Weekday = styled(TextComponent)`
  font-style: normal;
  font-weight: 500;
  font-size: 15px;
  color: #5c7585;
  padding-top: 3px;
`

const TasksText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  padding-top: 3px;
  color: #5c7585;
`

const HourGridBlock = styled(Animated.View)``

const TaskGridBlock = styled(Animated.View)``

const HourGrid = styled.View`
  flex: 1;
  flex-direction: row;
  position: relative;
`
const Grid = styled.View`
  border-left-width: 1px;
  border-top-width: 1px;
  border-color: #f2f5f5;
  position: relative;
  width: 100%;
`

const Hour = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #7590a1;
  margin-top: -8px;
  width: 60px;
`

const HourGridView = styled.View`
  flex-direction: column;
  padding-left: 16px;
  padding-bottom: 15px;
  position: relative;
`

const TaskGridView = styled.View`
  flex-direction: column;
  padding-left: 16px;
  padding-right: 16px;
  padding-bottom: 15px;
  margin-top:-2415px;

`

const TaskView = styled(Animated.View)`
  width:100%;
`

const TaskTitle = styled(Animated.Text)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #ffffff;
  padding-left: 8px;
  font-family: CeraPro-Medium;
  position:absolute;
  padding-left:84px;
`
