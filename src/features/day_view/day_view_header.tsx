import React from 'react'
import { DateTime } from 'luxon'
import CalendarSVG from '@src/assets/calendar.svg'
import MissionSVG from '@src/assets/mission.svg'
import styled from 'styled-components/native'
import { TouchableOpacity } from 'react-native'
import { TextComponent } from '@src/shared/text'

type Props = {
  date: DateTime
  onCalendarClick: () => void
  onFolderPress: () => void
}

export const DayViewHeader = ({
  date,
  onCalendarClick,
  onFolderPress,
}: Props) => {
  const currentMonth = React.useMemo(() => {
    return date.setLocale('ru-RU').monthLong.toLocaleLowerCase()
  }, [date])
  return (
    <Header>
      <Month>{currentMonth}</Month>
      <Icons>
      <StyledTouchable onPress={onFolderPress}>
          <MissionSVG />
        </StyledTouchable>

        <StyledTouchable onPress={onCalendarClick}>
          <CalendarSVG />
        </StyledTouchable>        
      </Icons>
    </Header>
  )
}

const StyledTouchable = styled(TouchableOpacity)`
  margin-left: 12px;
  justify-content: center;
`

const Icons = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`

const Header = styled.View`
  flex-direction: row;
  padding: 16px;
  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: #f2f5f5;
  justify-content: space-between;
`

const Month = styled(TextComponent)`
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #5c7585;
  line-height: 23px;
  padding-top: 4px;
`
