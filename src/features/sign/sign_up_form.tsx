import React from 'react'
import {
  TouchableWithoutFeedback,
  Keyboard,
  TouchableOpacity,
} from 'react-native'
import styled from 'styled-components/native'
import { ButtonComponent } from '@src/shared/button'
import { TextComponent } from '@src/shared/text'
import { Input } from '@src/shared/input'
import { useFormik } from 'formik'
import * as Yup from 'yup'
import Dialog from 'react-native-dialog'
import { apiCall, setApiToken } from '@src/api'
import AsyncStorage from '@react-native-community/async-storage'

type Props = {
  onSuccess: () => void
  onSignInPress: () => void
}

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .matches(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      'Неверный формат почты',
    )
    .required('введите почту'),
  password: Yup.string()
    .required('введите пароль')
    .min(6, 'пароль должен содержать 6 символов или больше'),
})

export const SignUpForm = ({ onSuccess, onSignInPress }: Props) => {
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    validateOnMount: false,
    validateOnBlur: false,
    validateOnChange: false,
    validationSchema,
    onSubmit: values => {
      const { email, password } = values
      return apiCall('POST', '/users/sign_up', { email, password })
        .then(response => {
          AsyncStorage.setItem(
            'token',
            response.data.data.attributes.api_token,
          ).then(() => {
            setApiToken(response.data.data.attributes.api_token).then(() =>
              onSuccess(),
            )
          })
        })
        .catch(e => {
          if (e?.response?.data?.errors) {
            formik.setErrors({ email: e.response.data.errors })
          }
        })
    },
  })

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Container>
        <Header>Регистрация</Header>
        <InputLabel>Email</InputLabel>
        <Input
          placeholder="Введите ваш e-mail"
          value={formik.values.email}
          onChangeText={value => formik.setFieldValue('email', value)}
          keyboardType={'email-address'}
          autoCompleteType={'email'}
          autoCapitalize={'none'}
          containerStyle={{ marginBottom: 12 }}
        />
        <InputLabel>Пароль</InputLabel>
        <Input
          placeholder="Придумайте пароль"
          value={formik.values.password}
          onChangeText={value => formik.setFieldValue('password', value)}
          secureTextEntry
          autoCompleteType={'password'}
          onSubmitEditing={formik.submitForm}
          containerStyle={{ marginBottom: 30 }}
        />
        <ButtonComponent
          onPress={formik.submitForm}
          disabled={formik.isSubmitting}
          fullWidth
          primary
          text="Зарегистрироваться"
        />
        <SignInBox>
          <SignInText>Уже есть аккаунт?</SignInText>
          <TouchableOpacity
            onPress={() => onSignInPress()}
            disabled={formik.isSubmitting}
          >
            <SignInButton>Войти</SignInButton>
          </TouchableOpacity>
        </SignInBox>
        <Dialog.Container visible={Object.keys(formik.errors).length !== 0}>
          <Dialog.Title>Ошибка</Dialog.Title>
          <Dialog.Description>
            {Object.values(formik.errors).join(', ')}
          </Dialog.Description>
          <Dialog.Button
            label="Закрыть"
            onPress={() => {
              formik.setErrors({})
            }}
          />
        </Dialog.Container>
      </Container>
    </TouchableWithoutFeedback>
  )
}

const Container = styled.View`
  padding: 0 16px;
`

const Header = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 24px;
  line-height: 35px;
  margin-top: 29%;
  color: #335366;
  text-align: center;
  margin-bottom: 17px;
`

const InputLabel = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #5c7585;
  margin-left: 25px;
  margin-bottom: 2px;
`

const SignInBox = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-top: 24px;
`

const SignInText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #335366;
  margin-right: 5px;
`

const SignInButton = styled(TextComponent)`
  color: #449e6f;
  text-decoration: underline;
  text-decoration-color: #449e6f;
`
