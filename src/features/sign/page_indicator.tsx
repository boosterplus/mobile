import React from 'react'
import styled from 'styled-components/native'
import { Animated } from 'react-native'

type Props = {
  initPageIndex?: number
  pageCount: number
  selectedPageIndex: number
}

export const PageIndicator = ({
  pageCount,
  selectedPageIndex,
  initPageIndex,
}: Props) => {
  const initIndex = React.useMemo(() => {
    return initPageIndex ?? 0
  }, [initPageIndex])

  const [prevIndex, setPrevIndex] = React.useState(initIndex)

  const animatedValues = React.useRef(
    Array(pageCount)
      .fill(0)
      .map((_, i) => {
        if (i === initIndex) {
          return new Animated.Value(1)
        } else {
          return new Animated.Value(0)
        }
      }),
  ).current

  const fadeAnimation = (animValue: Animated.Value, toValue: number) =>
    Animated.timing(animValue, {
      toValue,
      duration: 400,
      useNativeDriver: false,
    }).start()

  React.useEffect(() => {
    if (selectedPageIndex !== prevIndex) {
      fadeAnimation(animatedValues[prevIndex], 0)
      fadeAnimation(animatedValues[selectedPageIndex], 1)
    }
    setPrevIndex(selectedPageIndex)
  }, [selectedPageIndex])

  return (
    <IndicatorContainer>
      {Array(pageCount)
        .fill(0)
        .map((_, i) => {
          return (
            <Indicator key={i} pageCount={pageCount}>
              <Animated.View
                style={{
                  width: '100%',
                  height: '100%',
                  backgroundColor: '#449E6F',
                  opacity: animatedValues[i],
                  borderRadius: 14,
                }}
              />
            </Indicator>
          )
        })}
    </IndicatorContainer>
  )
}

const IndicatorContainer = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-around;
`

const Indicator = styled.View<{ pageCount: number }>`
  width:${props => `${80 / props.pageCount}%`}
  border-color: transparent;
  background-color: #fff;
  margin: 0 5px;
  height: 4px;
  border-radius: 100px;
`
