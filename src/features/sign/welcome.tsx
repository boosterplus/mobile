import React from 'react'
import { TextComponent } from '@src/shared/text'
import styled from 'styled-components/native'
import WelcomeSVG from '@src/assets/welcome.svg'

export const Welcome = () => {
  return (
    <Container>
      <InnerContainer>
        <Header>
          Добро пожаловать в экосистему
          <TextComponent style={{ color: '#449E6F' }}> Booster+!</TextComponent>
        </Header>
        <Subheader>Информационная система для управления жизнью</Subheader>
      </InnerContainer>
      <WelcomeSVG />
      <TipText>Смахните влево, чтобы продолжить</TipText>
    </Container>
  )
}

const Container = styled.View`
  align-items: center;
`

const InnerContainer = styled.View`
  margin: 20% 50px 0 50px;
`

const Header = styled(TextComponent)`
  font-weight: bold;
  font-size: 24px;
  line-height: 29px;
  text-align: center;
`

const Subheader = styled(TextComponent)`
  margin-top: 20px;
  margin-bottom: 5%;
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  text-align: center;
`

const TipText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  color: #5c7585;
`
