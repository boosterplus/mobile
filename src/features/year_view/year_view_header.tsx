import React from 'react'
import { DateTime } from 'luxon'
import MissionIcon24 from '@src/assets/mission.svg'
import styled from 'styled-components/native'
import { TextComponent } from '@src/shared/text'
import { Animated, TouchableOpacity } from 'react-native'

type Props = {
  date: DateTime
  onFolderPress: () => void
}

export const YearViewHeader = ({ date, onFolderPress }: Props) => {
  const [currentMonth, setCurrentMonth] = React.useState(
    date.setLocale('ru-RU').monthLong.toLocaleLowerCase(),
  )

  const animatedValue = React.useRef(new Animated.Value(1)).current
  const fadeAnim = (toValue: number) =>
    Animated.timing(animatedValue, {
      toValue,
      duration: 200,
      useNativeDriver: false,
    })

  React.useEffect(() => {
    fadeAnim(0).start(() => {
      setCurrentMonth(date.setLocale('ru-RU').monthLong.toLocaleLowerCase())
      fadeAnim(1).start()
    })
  }, [date.month])
  return (
    <>
      <Header>
        <Year>{date.year} / </Year>
        <Month style={{ opacity: animatedValue }}>{currentMonth}</Month>
        <TouchableOpacity
          style={{ marginLeft: 'auto' }}
          onPress={onFolderPress}
        >
          <MissionIcon24 />
        </TouchableOpacity>
      </Header>
      <Weekdays>
        <Weekday>п</Weekday>
        <Weekday>в</Weekday>
        <Weekday>с</Weekday>
        <Weekday>ч</Weekday>
        <Weekday>п</Weekday>
        <Weekday>с</Weekday>
        <Weekday>в</Weekday>
      </Weekdays>
    </>
  )
}

const Year = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  color: #5c7585;
  padding-top: 4px;
`

const Month = styled(Animated.Text)`
  font-family: CeraPro-Medium;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  color: #5c7585;
  padding-top: 4px;
`

const Header = styled.View`
  flex-direction: row;
  padding: 16px;
  border-bottom-width: 1px;
  align-items: center;
  border-bottom-color: #f2f5f5;
`

const Weekdays = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 12px 0;
  justify-content: space-around;
  border-bottom-width: 0.5px;
  border-bottom-color: #f2f5f5;
`

const Weekday = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 20px;
  text-align: center;
  color: #7590a1;
`
