import React from 'react'
import { DateTime } from 'luxon'
import ViewPager from '@react-native-community/viewpager'
import { DateObject } from '@src/typings/api'
import { YearViewMonth } from './year_view_month'
import { actionStore } from '@src/store/actionsStore'
import { Loader } from '@src/shared/loader'

type PagerProps = {
  date: DateTime
  onMonthChange: (index: number) => void
  onDayPress: (day: DateObject) => void
}

export const YearView = ({ date, onMonthChange, onDayPress }: PagerProps) => {
  const { state } = React.useContext(actionStore)
  const calendarMonths = React.useMemo(() => {
    const months = []
    for (let i = 0; i < 12; i++) {
      const monthActions = state.actions.filter(action => {
        const actionTime = DateTime.fromISO(action.attributes.start_date)
        return actionTime.year === date.year && actionTime.month === i + 1
      })
      months.push(
        <YearViewMonth
          actions={monthActions}
          onDayPress={onDayPress}
          key={i}
          month={i + 1}
          year={date.year}
          currentDayNumber={i + 1 === date.month ? date.day : null}
        />,
      )
    }
    return months
  }, [state.actions])

  if (state.isLoading) {
    return <Loader />
  }

  return (
    <ViewPager
      initialPage={date.month - 1}
      onPageSelected={e =>
        onMonthChange(e.nativeEvent.position + 1 - date.month)
      }
    >
      {calendarMonths}
    </ViewPager>
  )
}
