import React from 'react'
import { FlatList, Dimensions, View } from 'react-native'
import { DateTime, Interval } from 'luxon'
import { DateObject, ActionObject } from '@src/typings/api'
import styled from 'styled-components/native'
import { TextComponent } from '@src/shared/text'
import { actionTypes } from '@src/lib/util'

type MonthProps = {
  month: number
  year: number
  currentDayNumber: number | null
  onDayPress: (day: DateObject) => void
  actions: ActionObject[]
}

type DateCell = {
  year: number
  month: number
  day: number
  color: string
  actionColors: (string | null)[]
}

export const YearViewMonth = (p: MonthProps) => {
  const dateCells = React.useMemo(() => {
    const date = DateTime.local(p.year, p.month, 1)
    const cells = []

    const startDate = date.minus({ days: date.weekday - 1 })
    const endDate = startDate.plus({ days: 41 })
    const interval = Interval.fromDateTimes(startDate, endDate)

    for (let i = 0; i < interval.count('days'); i++) {
      const currentDate = startDate.plus({ days: i })
      const actionColors = p.actions.map(action => {
        const actionDate = DateTime.fromISO(action.attributes.start_date)
        if (
          actionDate.month === currentDate.month &&
          actionDate.day === currentDate.day
        ) {
          switch (action.attributes.process) {
            case 'Прокачивать':
              return actionTypes['Прокачивать'].color
            case 'Управлять':
              return actionTypes['Управлять'].color
            case 'Импакт':
              return actionTypes['Импакт'].color
            default:
              return null
          }
        } else {
          return null
        }
      })
      cells.push({
        year: currentDate.year,
        month: currentDate.month,
        day: currentDate.day,
        color: currentDate.month !== p.month ? '#DDDDDD' : '',
        actionColors: Array.from(new Set(actionColors)),
      })
    }
    return cells
  }, [p])

  const renderItem = ({ item, index }: { item: DateCell; index: number }) => {
    const isActiveDay =
      p.year === item.year &&
      p.month === item.month &&
      p.currentDayNumber === item.day
    return (
      <Cell
        onPress={() => p.onDayPress({ ...item, isActive: isActiveDay })}
        key={index}
        style={{ height: Dimensions.get('window').height / 7.64 }}
      >
        <View
          style={
            isActiveDay
              ? {
                  marginTop: -6,
                  alignItems: 'center',
                  justifyContent: 'center',
                  width: 28,
                  height: 28,
                  borderRadius: 28,
                  backgroundColor: '#449E6F',
                }
              : {}
          }
        >
          <CellText
            style={[
              item.color.length ? { color: item.color } : {},
              isActiveDay ? { color: 'white', lineHeight: 30 } : {},
            ]}
          >
            {item.day}
          </CellText>
        </View>
        <Actions>
          {item.actionColors.map(
            (c, i) => !!c && <Action key={i} style={{ backgroundColor: c }} />,
          )}
        </Actions>
      </Cell>
    )
  }

  return (
    <FlatList
      data={dateCells}
      renderItem={renderItem}
      numColumns={7}
      scrollEnabled={false}
      keyExtractor={({ day }) => day.toString()}
    />
  )
}

const Cell = styled.TouchableOpacity`
  flex: 1;
  border-width: 0.5px;
  border-color: #f2f5f5;
  padding: 8px 0;
  align-items: center;
  justify-content: space-between;
`

const CellText = styled(TextComponent)`
  text-align: center;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: #7590a1;
`

const Actions = styled.View`
  flex-direction: row;
  justify-content: center;
`

const Action = styled.View`
  width: 7px;
  min-width: 7px;
  height: 7px;
  min-height: 7px;
  border-radius: 7px;
  margin: 0 3px;
`
