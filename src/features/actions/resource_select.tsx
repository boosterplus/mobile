import React from 'react'
import { View } from 'react-native'
import { TextComponent } from '@src/shared/text'
import { powerUpTypes } from '@src/lib/util'
import { PowerUp } from '@src/typings/api'
import styled from 'styled-components/native'
import ArrowDownSVG from '@src/assets/arrow-down.svg'

type Props = {
  selectedOption: PowerUp
  onOptionSelect: (option: PowerUp) => void
}

export const ResourceSelect = ({ onOptionSelect, selectedOption }: Props) => {
  const [isOpened, setIsOpened] = React.useState(false)
  return (
    <Wrapper>
      <OptionView
        style={
          isOpened
            ? { borderBottomWidth: 1, borderColor: 'rgba(0, 0, 0, 0.1)' }
            : {}
        }
        activeOpacity={0.6}
        onPress={() => setIsOpened(!isOpened)}
      >
        {selectedOption.icon}
        <OptionTitle>{selectedOption.label}</OptionTitle>
        <ArrowDownSVG
          style={{
            position: 'absolute',
            top: 20,
            right: 16,

            transform: [{ rotate: isOpened ? '180deg' : '0deg' }],
          }}
        />
      </OptionView>
      {isOpened ? (
        <View
          style={{
            backgroundColor: 'white',
          }}
        >
          {powerUpTypes.map((powerup, index) => (
            <OptionView
              key={index}
              activeOpacity={0.6}
              onPress={() => {
                onOptionSelect(powerup)
                setIsOpened(false)
              }}
            >
              {powerup.icon}
              <OptionTitle>{powerup.label}</OptionTitle>
            </OptionView>
          ))}
        </View>
      ) : null}
    </Wrapper>
  )
}

const Wrapper = styled.View`
  margin-bottom: 5px;
`

const OptionView = styled.TouchableOpacity`
  flex-direction: row;
  padding: 16px;
  height: 50px;
  background-color: white;
`

const OptionTitle = styled(TextComponent)`
  margin-left: 10px;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: #335366;
`
