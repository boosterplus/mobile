import React from 'react'
import { TextComponent } from '@src/shared/text'
import { ActionObject } from '@src/typings/api'
import styled from 'styled-components/native'
import CrossSVG from '@src/assets/cross-white.svg'
import EditSVG from '@src/assets/edit-white.svg'
import CheckSVG from '@src/assets/check.svg'
import { TouchableOpacity } from 'react-native'
import { actionTypes } from '@src/lib/util'
import { apiCall } from '@src/api'
import { actionStore } from '@src/store/actionsStore'
import Toast from 'react-native-simple-toast'

type Props = {
  action: ActionObject
  onClose: () => void
  onEditClick: () => void
}

export const ActionHeader = (p: Props) => {
  const [isCompleted, setIsCompleted] = React.useState(
    p.action.attributes.status === 'completed',
  )
  const { dispatch } = React.useContext(actionStore)
  const onActionStatusClick = () => {
    const newStatus = isCompleted ? 'uncompleted' : 'completed'
    apiCall('POST', `/users/tasks/${p.action.id}/change_status`, {
      status: newStatus,
    })
      .then(response => {
        if (response.data) {
          dispatch({ type: 'editAction', payload: response.data.data })
          setIsCompleted(!isCompleted)
        }
      })
      .catch(e => Toast.show(e?.response?.data?.errors?.join('; '), 10))
  }
  return (
    <Wrapper>
      <Header
        style={{
          backgroundColor: actionTypes[p.action.attributes.process].color,
        }}
      >
        <HeaderText>{p.action.attributes.title}</HeaderText>
        <Controls>
          <TouchableOpacity onPress={p.onEditClick}>
            <EditSVG />
          </TouchableOpacity>
          <TouchableOpacity style={{ marginLeft: 16 }} onPress={p.onClose}>
            <CrossSVG />
          </TouchableOpacity>
        </Controls>
      </Header>
      <SubheaderText>Статус задачи</SubheaderText>
      <StatusView>
        <StatusText>{isCompleted ? 'Выполнено' : 'Не выполнено'}</StatusText>
        <TouchableOpacity activeOpacity={0.6} onPress={onActionStatusClick}>
          <StatusCheckbox>{isCompleted ? <CheckSVG /> : null}</StatusCheckbox>
        </TouchableOpacity>
      </StatusView>
    </Wrapper>
  )
}

const Wrapper = styled.View`
  height: 100%;
  border-radius: 8px;
  background-color: #f8fafc;
`

const Header = styled.View`
  position: relative;
  padding: 28px 16px;
  border-top-width: 2px;
  border-right-width: 2px;
  border-top-color: #f8fafc;
  border-right-color: #f8fafc;
  border-top-left-radius:8px;
  border-top-right-radius:8px;
`

const Controls = styled.View`
  position: absolute;
  top: 8px;
  right: 8px;
  flex-direction: row;
`

const HeaderText = styled(TextComponent)`
  font-style: normal;
  font-weight: bold;
  font-size: 22px;
  color: #ffffff;
`

const SubheaderText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: rgba(51, 83, 102, 0.6);
  padding: 12px 0 5px 16px;
`

const StatusView = styled.View`
  padding: 16px;
  background-color: white;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const StatusText = styled(TextComponent)`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #335366;
`

const StatusCheckbox = styled.View`
  border-width: 1.5px;
  border-color: #449e6f;
  min-width: 22px;
  min-height: 22px;
  width: 22px;
  height: 22px;
  border-radius: 22px;
  justify-content: center;
  align-items: center;
`
