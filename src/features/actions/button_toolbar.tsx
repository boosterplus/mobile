import React from 'react'
import styled from 'styled-components/native'
import { TextComponent } from '@src/shared/text'
import { TouchableOpacity } from 'react-native'
import ActionSVG from '@src/assets/action.svg'
import MissionSVG from '@src/assets/mission.svg'
import PlusSVG from '@src/assets/plus.svg'

type Props = {
  onAddActionPress: () => void
  onAddMissionPress: () => void
}

export const ButtonToolbar = ({
  onAddActionPress,
  onAddMissionPress,
}: Props) => {
  const [areActionsVisible, setActionsVisible] = React.useState(false)
  return (
    <Wrapper>
      {areActionsVisible && (
        <>
          <ActionWithTitle
            activeOpacity={0.5}
            onPress={() => {
              setActionsVisible(false)
              onAddActionPress()
            }}
          >
            <ActionTitle>Действие</ActionTitle>
            <ActionButton
              style={{
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 4,
                },
                shadowOpacity: 0.3,
                shadowRadius: 4.65,

                elevation: 8,
              }}
            >
              <ActionSVG />
            </ActionButton>
          </ActionWithTitle>
          <ActionWithTitle
            activeOpacity={0.5}
            onPress={() => {
              setActionsVisible(false)
              onAddMissionPress()
            }}
          >
            <ActionTitle>Миссия</ActionTitle>
            <ActionButton
              style={{
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 4,
                },
                shadowOpacity: 0.3,
                shadowRadius: 4.65,

                elevation: 8,
              }}
            >
              <MissionSVG />
            </ActionButton>
          </ActionWithTitle>
        </>
      )}
      <TouchableOpacity
        activeOpacity={0.5}
        onPress={() => setActionsVisible(!areActionsVisible)}
      >
        <AddButton
          style={{
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.3,
            shadowRadius: 4.65,

            elevation: 8,
          }}
        >
          <PlusSVG />
        </AddButton>
      </TouchableOpacity>
    </Wrapper>
  )
}

const Wrapper = styled.View`
  position: absolute;
  bottom: 32px;
  right: 16px;
  align-items: flex-end;
`

const AddButton = styled.View`
  width: 50px;
  height: 50px;
  border-radius: 50px;
  background-color: #449e6f;
  align-items: center;
  justify-content: center;
`

const ActionButton = styled.View`
  width: 50px;
  height: 50px;
  border-radius: 50px;
  background-color: white;
  align-items: center;
  justify-content: center;
`

const ActionWithTitle = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  margin-bottom: 12px;
`

const ActionTitle = styled(TextComponent)`
  margin-right: 10px;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #5c7585;
`
