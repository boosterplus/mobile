import React from 'react'
import { ActivityIndicator } from 'react-native'
import styled from 'styled-components/native'

export const Loader = () => {
  return (
    <LoaderView>
      <ActivityIndicator size="large" color="#449E6F" />
    </LoaderView>
  )
}

const LoaderView = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: white;
`
