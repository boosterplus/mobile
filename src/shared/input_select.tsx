import React from 'react'
import styled from 'styled-components/native'
import RNPickerSelect from 'react-native-picker-select'
import { StyleProp, ViewStyle } from 'react-native'
import ArrowDownSVG from '@src/assets/arrow-down.svg'

type Props = {
  options: string[]
  selectedOption: string
  onOptionSelect: (option: string) => void
  style?: StyleProp<ViewStyle>
  disabled?: boolean
}

type OptionsProps = {
  options: { label: string; value: string }[]
  selectedOption: string
  onOptionSelect: (value: string) => void
  style?: StyleProp<ViewStyle>
  disabled?: boolean
}

export const InputSelect = (p: Props) => {
  return (
    <Container style={[p.style]}>
      <RNPickerSelect
        style={{
          inputIOS: { ...pickerStylesObject, fontWeight: 'normal' },
          inputAndroid: { ...pickerStylesObject, fontWeight: 'normal' },
          placeholder: {
            color: 'rgba(0, 0, 0, 0.2)',
            fontSize: 16,
          },
          iconContainer: {
            top: 22,
            right: 16,
          },
        }}
        disabled={p.disabled}
        useNativeAndroidPickerStyle={false}
        placeholder={{}}
        value={p.selectedOption}
        onValueChange={option => p.onOptionSelect(option)}
        items={p.options.map(o => ({ label: o, value: o }))}
        doneText={'Готово'}
        Icon={() => <ArrowDownSVG />}
      />
    </Container>
  )
}

const Container = styled.View`
  max-width: 100%;
`

const pickerStylesObject = {
  fontFamily: 'CeraPro-Medium',
  fontSize: 16,
  paddingVertical: 15,
  backgroundColor: 'white',
  paddingHorizontal: 15,
  color: '#335366',
  height: 50,
}

export const InputSelectOption = (p: OptionsProps) => {
  return (
    <Container style={[p.style]}>
      <RNPickerSelect
        style={{
          inputIOS: { ...pickerStylesObject, fontWeight: 'normal' },
          inputAndroid: { ...pickerStylesObject, fontWeight: 'normal' },
          placeholder: {
            color: 'rgba(0, 0, 0, 0.2)',
            fontSize: 16,
          },
          iconContainer: {
            top: 22,
            right: 16,
          },
        }}
        disabled={p.disabled}
        useNativeAndroidPickerStyle={false}
        placeholder={{}}
        value={p.selectedOption}
        onValueChange={value => p.onOptionSelect(value)}
        items={p.options}
        doneText={'Готово'}
        Icon={() => <ArrowDownSVG />}
      />
    </Container>
  )
}
