import React from 'react'
import { Text, TextProps } from 'react-native'

export const TextComponent: React.FC<TextProps> = props => {
  const defaultStyle = {
    fontFamily: 'CeraPro-Medium',
    color: '#335366',
  }
  const incomingStyle = Array.isArray(props.style) ? props.style : [props.style]
  return (
    <Text {...props} style={[defaultStyle, ...incomingStyle]}>
      {props.children}
    </Text>
  )
}
