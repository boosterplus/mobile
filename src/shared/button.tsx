import React from 'react'
import {
  NativeTouchEvent,
  NativeSyntheticEvent,
  ViewStyle,
  StyleProp,
  TextStyle,
  TouchableOpacity,
} from 'react-native'
import styled from 'styled-components/native'
import { TextComponent } from './text'

type Props = {
  text: string
  onPress: (ev: NativeSyntheticEvent<NativeTouchEvent>) => void
  fullWidth?: boolean
  disabled?: boolean
  danger?: boolean
  primary?: boolean
  style?: StyleProp<ViewStyle>
  textStyle?: StyleProp<TextStyle>
}

export const ButtonComponent = (p: Props) => {
  return (
    <TouchableOpacity
      onPress={p.onPress}
      disabled={p.disabled}
      activeOpacity={0.5}
      style={p.fullWidth && { width: '100%' }}
    >
      <Container
        style={[
          p.style,
          p.primary && { backgroundColor: '#449e6f' },
          p.danger && { borderWidth:1,borderColor: '#FF6464', backgroundColor: 'ffffff'},
          p.disabled && { opacity: 0.4 },
          p.fullWidth && { width: '100%' },
        ]}
      >
        <ButtonText style={p.textStyle}>{p.text}</ButtonText>
      </Container>
    </TouchableOpacity>
  )
}

const Container = styled.View`
  display: flex;
  height: 60px;
  justify-content: center;
  align-items: center;
  border-radius: 8px;
  padding: 7px 15px;
`

const ButtonText = styled(TextComponent)`
  font-size: 18px;
  color: #ffffff;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 23px;
  text-align: center;
`
