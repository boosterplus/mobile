import React from 'react'
import styled from 'styled-components/native'
import { ViewStyle, StyleProp, TextInputProps } from 'react-native'
import { TextComponent } from './text'

type Props = TextInputProps & {
  onChangeText: (text: string) => void
  errorMessage?: string | null
  containerStyle?: StyleProp<ViewStyle>
}

export const Input = (p: Props) => {
  return (
    <Container style={[p.containerStyle, { width: '100%' }]}>
      <StyledInput
        style={p.errorMessage?.length ? { borderColor: '#FF8686' } : {}}
        placeholderTextColor={'rgba(0, 0, 0, 0.2)'}
        {...p}
      />
      {p.errorMessage?.length ? (
        <ErrorMessage>{p.errorMessage}</ErrorMessage>
      ) : null}
    </Container>
  )
}

const Container = styled.View`
  position: relative;
`

const StyledInput = styled.TextInput`
  font-family: 'CeraPro-Medium';
  width: 100%;
  height: 60px;
  color: #335366;
  border-radius: 4px;
  border-width: 1px;
  border-color: rgba(0, 0, 0, 0.1);
  font-size: 15px;
  padding: 0 16px;
  border-radius: 100px;
  font-style: normal;
  font-weight: normal;
  background-color: white;
`

const ErrorMessage = styled(TextComponent)`
  position: absolute;
  top: -20px;
  left: 25px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;

  text-align: center;

  color: #ff8686;
`
