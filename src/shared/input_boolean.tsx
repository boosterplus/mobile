import React from 'react'
import styled from 'styled-components/native'
import { ViewStyle, StyleProp } from 'react-native'
import { TextComponent } from './text'

import { TouchableOpacity } from 'react-native'

import CheckCircle from '@src/assets/checkboxCircle.svg'
import Check from '@src/assets/checkboxCheck.svg'

type Props = {
  value: boolean
  label: string
  onChange: (value: boolean) => void
  errorMessage?: string | null
  containerStyle?: StyleProp<ViewStyle>
}

export const InputBoolean = (p: Props) => {
  return (
    <Container style={[p.containerStyle, { width: '100%' }]}>
      <TouchableOpacity
        onPressOut={() => p.onChange(!p.value)}
        activeOpacity={0.5}
      >
        <StyledInput
          style={p.errorMessage?.length ? { borderColor: '#FF8686' } : {}}
        >
          <TextComponent style={{ fontWeight: 'normal' }}>
            {p.label}
          </TextComponent>
          <CheckBox>
            <CheckCircle style={{ position: 'absolute' }} />
            {p.value && (
              <Check style={{ position: 'absolute', marginLeft: 6 }} />
            )}
          </CheckBox>
        </StyledInput>
        {p.errorMessage?.length ? (
          <ErrorMessage>{p.errorMessage}</ErrorMessage>
        ) : null}
      </TouchableOpacity>
    </Container>
  )
}

const Container = styled.View`
  position: relative;
`

const StyledInput = styled.View`
  width: 100%;
  height: 60px;
  color: #335366;
  padding: 0 16px;

  background-color: white;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

const CheckBox = styled.View`
  position: relative;
  flex-direction: row;
  width: 22px;
  align-items: center;
`

const ErrorMessage = styled(TextComponent)`
  position: absolute;
  top: -20px;
  left: 25px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;

  text-align: center;

  color: #ff8686;
`
