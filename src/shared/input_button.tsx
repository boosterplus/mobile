import React from 'react'
import styled from 'styled-components/native'
import { ViewStyle, StyleProp } from 'react-native'
import { TextComponent } from './text'

import { TouchableOpacity } from 'react-native'

type Props = {
  label: string
  containerStyle?: StyleProp<ViewStyle>
  onPress?: () => void
}

export const InputButton: React.FC<Props> = ({
  children,
  onPress,
  label,
  containerStyle,
}) => {
  return (
    <Container style={[containerStyle, { width: '100%' }]}>
      <TouchableOpacity onPress={onPress} activeOpacity={0.5}>
        <StyledInput>
          <TextComponent style={{ fontWeight: 'normal' }}>
            {label}
          </TextComponent>
          {children}
        </StyledInput>
      </TouchableOpacity>
    </Container>
  )
}

const Container = styled.View`
  position: relative;
`

const StyledInput = styled.View`
  width: 100%;
  height: 60px;
  color: #335366;
  padding: 0 16px;

  background-color: white;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`
