export type SignUpResult = {
  id: string
  email: string
  created_at: number
  updated_at: number
  name: string
  avatar: any
  api_token: string
}

export type User = {
  id: string
  name: string
  email: string
  avatar: any
}

export type DateObject = {
  isActive?: boolean
  year: number
  month: number
  day: number
}

export type ActionObject = {
  id: string // in tasks endpoint integer is returned
  type: 'task'
  attributes: {
    start_date: string
    end_repetition_date: string | null
    description?: string | null
    duration: number
    process: string
    process_scope: string
    title: string
    status: string | null
    frequency: string | null
  }
}

export type PowerUp = {
  label: string
  value: string
  icon: JSX.Element
}

export type MissionObject = {
  type: 'mission'
  id: string
  attributes: {
    title: string
    process: string
    process_scope: string
    start_date: string
    end_date?: string
    without_deadline: boolean
    created_at: string
    updated_at: string
    tasks: { data: ActionObject[] }
  }
}

export type UserProfile = {
  type: 'user'
  id: string
  attributes: {
    email: string
    name: string | null
    missions: { data: MissionObject[] }
    tasks: { data: ActionObject[] }
  }
}

export type UserStore = {
  name: string | null
  email: string
}

export type MissionPayload = {
  title: string
  process: string
  process_scope: string
  start_date: string
  end_date?: string
  without_deadline: boolean
  task_ids: string[]
}

export type ActionPayload = {
  title: string
  process: string
  process_scope: string | undefined
  duration: number
  start_date: string
  frequency?: string | null
  status?: string | null
  mission?: string | null
}
