export const init = 'Init'

export const app = 'App'

export const sign = 'Sign'

export const calendar = 'Calendar'

export const calendarView = 'CalendarView'

export const addAction = 'AddAction'

export const editAction = 'EditAction'

export const yearView = 'YearView'

export const dayView = 'DayView'

export const welcomeView = 'WelcomeView'

export const addMission = 'AddMission'

export const folderView = 'FolderView'

export const missionsView = 'FolderMissionsView'
