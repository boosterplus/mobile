import React from 'react'
import LifetimeSVG from '@src/assets/lifetime.svg'
import HealthSVG from '@src/assets/health.svg'
import EnergySVG from '@src/assets/energy.svg'
import AbilitiesSVG from '@src/assets/abilities.svg'
import FinanceSVG from '@src/assets/finance.svg'
import ConnectionSVG from '@src/assets/connection.svg'
import InformationSVG from '@src/assets/information.svg'
import TechnologySVG from '@src/assets/technology.svg'
import { PowerUp } from '@src/typings/api'

export const actionTypes: {
  [key: string]: { label: string; value: string; color: string }
} = {
  Прокачивать: {
    label: 'Прокачивать',
    value: 'level_up',
    color: '#FF6969',
  },
  Импакт: {
    label: 'Импакт',
    value: 'impact',
    color: '#BD95FF',
  },
  Управлять: {
    label: 'Управлять',
    value: 'manage',
    color: '#FFC47D',
  },
}

export const powerUpTypes: PowerUp[] = [
  {
    label: 'Время жизни',
    value: 'lifetime',
    icon: <LifetimeSVG />,
  },
  {
    label: 'Здоровье',
    value: 'health',
    icon: <HealthSVG />,
  },
  {
    label: 'Энергия',
    value: 'energy',
    icon: <EnergySVG />,
  },
  {
    label: 'Способности',
    value: 'abilities',
    icon: <AbilitiesSVG />,
  },
  {
    label: 'Финансы',
    value: 'finance',
    icon: <FinanceSVG />,
  },
  {
    label: 'Связи',
    value: 'connection',
    icon: <ConnectionSVG />,
  },
  {
    label: 'Информация',
    value: 'information',
    icon: <InformationSVG />,
  },
  {
    label: 'Технологии',
    value: 'technology',
    icon: <TechnologySVG />,
  },
]
