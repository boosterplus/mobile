import React from 'react'
import { Animated } from 'react-native'
import { State } from 'react-native-gesture-handler'

export const usePinch = (initScale: number) => {
  const baseScale = React.useRef(
    React.useMemo(() => new Animated.Value(initScale), []),
  ).current
  const pinchScale = React.useRef(
    React.useMemo(() => new Animated.Value(1), []),
  ).current
  const scale = React.useRef(
    React.useMemo(() => Animated.multiply(baseScale, pinchScale), []),
  ).current
  const pinchPosition = React.useRef(
    React.useMemo(() => new Animated.Value(1), []),
  ).current

  const lastScale = React.useRef({ value: 1 }).current

  const onPinchEvent = React.useCallback(
    Animated.event<{ scale: number }>(
      [
        {
          nativeEvent: { scale: pinchScale },
        },
      ],
      { useNativeDriver: false },
    ),
    [],
  )

  const onPinchStateChange = React.useCallback(event => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      const newScaleValue = lastScale.value * event.nativeEvent.scale
      lastScale.value = newScaleValue < 0.2 ? 0.2 : newScaleValue > 2 ? 2 : newScaleValue
      baseScale.setValue(lastScale.value)
      pinchScale.setValue(1)
      pinchPosition.setValue(event.nativeEvent.focalY)
    }
  }, [])

  return { onPinchEvent, onPinchStateChange, scale, pinchPosition }
}
