import React, { useEffect, useReducer, useState } from 'react'
import {
  ActionObject,
  MissionObject,
  UserProfile,
  UserStore,
} from '@src/typings/api'
import { apiInstance, clearApiToken, getProfile } from '@src/api'
import Dialog from 'react-native-dialog'
import { useNavigation } from '@react-navigation/native'
import { sign } from '@src/lib/routes'

type State = {
  actions: ActionObject[]
  missions: MissionObject[]
  profile: UserStore | null
  isLoading: boolean
}

type StateAction =
  | { type: 'reset' }
  | { type: 'setLoading'; payload: boolean }
  | { type: 'loadNewActions'; payload: ActionObject[] }
  | { type: 'loadNewMissions'; payload: MissionObject[] }
  | { type: 'loadProfile'; payload: UserProfile }
  | { type: 'addAction'; payload: ActionObject[] }
  | { type: 'editAction'; payload: ActionObject[] }
  | { type: 'deleteAction'; payload: { id: string } }
  | { type: 'addMission'; payload: MissionObject }
  | { type: 'editMission'; payload: MissionObject }
  | { type: 'deleteMission'; payload: { id: string } }

type Context = {
  state: State
  dispatch: React.Dispatch<StateAction>
}

const initState = { actions: [], missions: [], profile: null, isLoading: true }

const actionStore = React.createContext<Context>({
  state: initState,
  dispatch: () => {},
})
const { Provider } = actionStore

const useActionStore = React.useContext.bind(
  undefined,
  actionStore as any,
) as () => Context

/// moved reducer func out of render function so it remains pure
const actionsReducer = (currentState: State, action: StateAction) => {
  switch (action.type) {
    case 'reset':
      return { actions: [], missions: [], profile: null, isLoading: true }
    case 'setLoading':
      return { ...currentState, isLoading: action.payload }
    case 'loadProfile':
      return {
        ...currentState,
        profile: {
          name: action.payload.attributes.name,
          email: action.payload.attributes.email,
        },
      }
    case 'loadNewActions':
      return { ...currentState, actions: action.payload }
    case 'loadNewMissions':
      return { ...currentState, missions: action.payload }
    case 'addAction':
      return {
        ...currentState,
        actions: [...currentState.actions, ...action.payload],
        isLoading: false,
      }
    case 'editAction':
      const prevActions = currentState.actions
      action.payload.forEach(editedAction => {
        const editedActionIndex = prevActions.findIndex(
          a => a.id === editedAction.id,
        )
        if (editedActionIndex > -1) {
          prevActions[editedActionIndex] = { ...editedAction }
        }
      })
      return {
        ...currentState,
        actions: [...prevActions],
        isLoading: false,
      }
    case 'deleteAction':
      const newActions = currentState.actions
      const index = newActions.findIndex(a => a.id === action.payload.id)
      if (index > -1) {
        newActions.splice(index, 1)
      }
      return { ...currentState, actions: newActions, isLoading: false }
    case 'addMission':
      return {
        ...currentState,
        missions: [...currentState.missions, action.payload],
        isLoading: false,
      }
    case 'editMission':
      const editMissions = currentState.missions

      const editedActionIndex = editMissions.findIndex(
        m => m.id === action.payload.id,
      )

      if (editedActionIndex > -1) {
        editMissions[editedActionIndex] = action.payload
      }
      return { ...currentState, missions: [...editMissions], isLoading: false }

    case 'deleteMission':
      const missions = currentState.missions
      const deleteIndex = missions.findIndex(m => m.id === action.payload.id)
      if (deleteIndex > -1) {
        missions.splice(deleteIndex, 1)
      }
      return {
        ...currentState,
        missions: [...missions],
        isLoading: false,
      }
    default:
      throw new Error('No such store action')
  }
}

const ActionsStateProvider: React.FC = ({ children }) => {
  const navigation = useNavigation()
  const [serverError, setServerError] = useState<string | null>(null)

  const [state, dispatch] = useReducer(actionsReducer, initState)

  if (__DEV__) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
      console.warn('Store Init')
      return () => {
        console.warn('Store Destruction')
      }
    }, [])
  }

  useEffect(() => {
    const interceptor = apiInstance.interceptors.response.use(
      r => r,
      e => {
        console.warn(
          'request',
          e.request._method,
          e.request._url,
          ' returned ',
          e.response.status,
        )
        e.response.data && console.warn('response data', e.response.data)
        e.response.errors &&
          console.warn('response errors', e.response.data.errors)
        const status = e?.response?.status ?? ''
        if (status === 401 || status === 403) {
          clearApiToken().then(() => {
            dispatch({ type: 'reset' })
            // we use navigation reset, so all prev screen are unmouted
            navigation.reset({ index: 0, routes: [{ name: sign }] })
          })
          return
        } else if (e.response.data.errors) {
          setServerError(e.response.data.errors)
        } else if (e.isAxiosError) {
          setServerError('При выполнении запроса произошла ошибка.')
        }
        throw e
      },
    )
    return () => apiInstance.interceptors.response.eject(interceptor)
  }, [])

  useEffect(() => {
    getProfile()
      .then(data => {
        dispatch({
          type: 'loadNewMissions',
          payload: data.attributes.missions.data,
        })
        dispatch({
          type: 'loadNewActions',
          payload: data.attributes.tasks.data,
        })
        dispatch({ type: 'loadProfile', payload: data })
      })
      .finally(() => dispatch({ type: 'setLoading', payload: false }))
  }, [])

  return (
    <Provider value={{ state, dispatch }}>
      {children}
      <Dialog.Container visible={!!serverError?.length}>
        <Dialog.Title>Ошибка</Dialog.Title>
        <Dialog.Description>{serverError ?? ''}</Dialog.Description>
        <Dialog.Button label="Закрыть" onPress={() => setServerError(null)} />
      </Dialog.Container>
    </Provider>
  )
}

export { actionStore, ActionsStateProvider, useActionStore }
