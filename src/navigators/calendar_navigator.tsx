import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import {
  addAction,
  yearView,
  dayView,
  editAction,
  addMission,
  folderView,
  missionsView,
} from '@src/lib/routes'
import { YearViewScreen } from '@src/screens/year_view_screen'
import { AddActionScreen } from '@src/screens/add_action_screen'
import { FolderScreen } from '@src/screens/folder_screen'
import { DayViewScreen } from '@src/screens/day_view_screen'
import { DateObject, ActionObject } from '@src/typings/api'
import { ActionsStateProvider } from '@src/store/actionsStore'
import { EditActionScreen } from '@src/screens/edit_action_screen'
import { AddMissionScreen } from '@src/screens/mission_screen'
import { MissionsScreen } from '@src/screens/missions_screen'
export type CalendarStackParamList = {
  [yearView]: undefined
  [dayView]: DateObject
  [addAction]: { date: DateObject; fromYearView?: boolean }
  [editAction]: { action: ActionObject }
  [addMission]?: { date?: DateObject; mission_id?: string }
  [folderView]: undefined
  [missionsView]: undefined
}

const Stack = createStackNavigator<CalendarStackParamList>()

export const CalendarNavigator = () => {
  return (
    <ActionsStateProvider>
      <Stack.Navigator initialRouteName={yearView} mode="modal">
        <Stack.Screen
          name={yearView}
          component={YearViewScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={dayView}
          component={DayViewScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name={addAction}
          component={AddActionScreen}
          options={{
            headerTitle: 'Добавить действие',
            headerStyle: {
              backgroundColor: '#f8fbfc',
              shadowColor: 'transparent',
            },
            headerTitleStyle: {
              fontFamily: 'CeraPro-Medium',
              color: '#335366',
            },
          }}
        />
        <Stack.Screen
          name={editAction}
          component={EditActionScreen}
          options={{
            headerTitle: 'Изменить действие',
            headerStyle: {
              backgroundColor: '#f8fbfc',
              shadowColor: 'transparent',
            },
            headerTitleStyle: {
              fontFamily: 'CeraPro-Medium',
              color: '#335366',
            },
          }}
        />
        <Stack.Screen
          name={addMission}
          component={AddMissionScreen}
          options={{
            headerTitle: 'Миссия',
            headerStyle: {
              backgroundColor: '#f8fbfc',
              shadowColor: 'transparent',
            },

            headerTitleStyle: {
              fontFamily: 'CeraPro-Medium',
              color: '#335366',
            },
          }}
        />
        <Stack.Screen
          name={folderView}
          component={FolderScreen}
          options={{
            headerTitle: 'Папки',
            headerStyle: {
              backgroundColor: '#f8fbfc',
              shadowColor: 'transparent',
            },

            headerTitleStyle: {
              fontFamily: 'CeraPro-Medium',
              color: '#335366',
            },
            headerBackTitle: 'Назад',
          }}
        />
        <Stack.Screen
          name={missionsView}
          component={MissionsScreen}
          options={{
            headerTitle: 'Миссии',
            headerStyle: {
              backgroundColor: '#f8fbfc',
              shadowColor: 'transparent',
            },

            headerTitleStyle: {
              fontFamily: 'CeraPro-Medium',
              color: '#335366',
            },
            headerBackTitle: 'Назад',
          }}
        />
      </Stack.Navigator>
    </ActionsStateProvider>
  )
}
