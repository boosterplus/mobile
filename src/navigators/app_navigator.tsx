import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { sign, welcomeView, calendar } from '@src/lib/routes'
import { SignScreen } from '@src/screens/sign_screen'
import { WelcomeScreen } from '@src/screens/welcome_screen'
import { RouteProp } from '@react-navigation/native'
import { RootStackParamList } from './root_navigator'
import { CalendarNavigator } from './calendar_navigator'

export type AppStackParamList = {
  [sign]: undefined
  [calendar]: undefined
  [welcomeView]: undefined
}

type NavigationProps = {
  route: RouteProp<RootStackParamList, 'App'>
}

const Stack = createStackNavigator<AppStackParamList>()

export const AppNavigator = ({ route }: NavigationProps) => {
  const initRouteName = React.useMemo(() => {
    if (route.params.hasToken) {
      return calendar
    } else if (!route.params.tutorialComplete) {
      return welcomeView
    } else {
      return sign
    }
  }, [route.params.hasToken])

  return (
    <Stack.Navigator initialRouteName={initRouteName}>
      <Stack.Screen
        name={sign}
        component={SignScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={welcomeView}
        component={WelcomeScreen}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name={calendar}
        component={CalendarNavigator}
        options={{ gestureEnabled: false, headerShown: false }}
      />
    </Stack.Navigator>
  )
}
