import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { InitScreen } from '@src/screens/init_screen'
import { init, app } from '@src/lib/routes'
import { AppNavigator } from './app_navigator'

export type RootStackParamList = {
  [init]: undefined
  [app]: {
    hasToken: boolean
    tutorialComplete: boolean
  }
}

const Stack = createStackNavigator<RootStackParamList>()

const RootNavigator = () => (
  <Stack.Navigator initialRouteName={init}>
    <Stack.Screen
      name={init}
      component={InitScreen}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={app}
      component={AppNavigator}
      options={{ headerShown: false }}
    />
  </Stack.Navigator>
)

export default RootNavigator
