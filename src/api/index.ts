import axios, { AxiosRequestConfig } from 'axios'
import { API_URI } from '@src/lib/env'
// import { useState, useEffect } from 'react'
import AsyncStorage from '@react-native-community/async-storage'

import {
  ActionObject,
  MissionObject,
  MissionPayload,
  ActionPayload,
  UserProfile,
} from '@src/typings/api'

export const apiInstance = axios.create({
  baseURL: `${API_URI}/api`,
  timeout: 5000,
})

export const setApiToken = (token: string) => {
  apiInstance.defaults.headers.common.Authorization = `Bearer ${token}`
  return AsyncStorage.setItem('token', token)
}

export const clearApiToken = () => {
  apiInstance.defaults.headers.common.Authorization = ''
  return AsyncStorage.removeItem('token')
}

export const apiCall = (
  method?: AxiosRequestConfig['method'],
  url?: string,
  data?: any,
) => {
  return apiInstance.request({
    method,
    url,
    data,
  })
}

export const getActions = () => {
  return apiCall('GET', '/users/tasks').then(
    result => result.data.data as ActionObject[],
  )
}

export const createAction = (task: ActionPayload) => {
  return apiCall('POST', '/users/tasks', { task }).then(
    response => response.data.data as ActionObject[],
  )
}

export const editAction = (id: string, task: ActionPayload) => {
  return apiCall('PUT', `/users/tasks/${id}`, { task }).then(
    response => response.data.data as ActionObject[],
  )
}

export const getMissions = () => {
  return apiCall('GET', '/users/missions').then(
    result => result.data.data as MissionObject[],
  )
}

export const createMission = (data: MissionPayload) => {
  return apiCall('POST', '/users/missions', { mission: data }).then(
    result => result.data.data[0] as MissionObject,
  )
}

export const editMission = (mission_id: string, data: MissionPayload) => {
  return apiCall('PUT', `/users/missions/${mission_id}`, {
    mission: data,
  }).then(result => result.data.data[0] as MissionObject)
}

export const deleteMission = (mission_id: string) => {
  return apiCall('DELETE', `/users/missions/${mission_id}`).then(
    result => result.data as any,
  )
}

export const getProfile = () => {
  return apiCall('GET', '/users/profile').then(
    res => res.data.data as UserProfile,
  )
}

// export const useFetchData = <T>(url: string) => {
//   const [loading, setLoading] = useState(true)
//   const [error, setError] = useState<string | null>(null)
//   const [data, setData] = useState<T | null | undefined>(undefined)

//   useEffect(() => {
//     if (loading) {
//       apiInstance
//         .get<T>(url)
//         .then(response => {
//           setData(response.data)
//         })
//         .catch(e => setError(e))
//         .then(() => setLoading(false))
//     }
//   }, [url, loading])

//   return { data, error, loading }
// }
