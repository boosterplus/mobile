import React from 'react'
import RootNavigator from '@src/navigators/root_navigator'
import { NavigationContainer } from '@react-navigation/native'
import { SafeAreaView } from 'react-native'

const App = () => (
  <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  </SafeAreaView>
)

export default App
